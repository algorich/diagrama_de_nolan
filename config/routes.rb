# -*- encoding : utf-8 -*-

DiagramaDeNolan::Application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'

  mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'

#= 1. user routes ==============================================================
  devise_for :users

#= 2. lecture request routes ===========================================================
  get "/solicitar-palestra" => "site#lecture_request", :as => :lecture_request
  post "/solicitar-palestra" => "site#send_lecture_request", :as => :send_lecture_request

#= 3. questionnaire routes =====================================================
  get  '/questionario' => 'site#responses_new', :as => :questionnaire
  post '/questionario/validar_respostas' => 'site#validate_responses', :as => :validate_responses
  post '/questionario' => 'site#responses_create', :as => :responses_create
  get  '/questionario/resultado' => 'site#result', :as => :result

#= 4. result routes ============================================================
  get '/questionario/:position' => 'site#result_redirector', :as => :result_redirector

#= 5. page routes ==============================================================
  get '/cities' => 'cities#index'

#= 6. page routes ==============================================================
  get '/:page' => 'site#page', :as => :page

  root :to => 'site#index'
end
