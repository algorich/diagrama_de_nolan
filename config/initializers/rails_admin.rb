# -*- encoding : utf-8 -*-

RailsAdmin.config do |config|

  config.current_user_method { current_user } #auto-generated

  config.authorize_with :cancan

  config.excluded_models = [Response]

  config.model Configuration do
    weight 1

    edit do
      group :email do
        active false
        label 'Configurações de email'

        field :email
        help 'Conta de email que receberá as solicitações de palestras.'
      end

      group :info_search do
        active false

        label 'Informações para buscadores'

        field :google_analytics

        field :google_analytics_page_tracker

        field :keywords do
           help 'Separadas por vírgula. Recomendável no máximo 10 palavras chave.'
        end

        field :description do
          help 'Descrição utilizada pelos buscadores. Recomendável até 160 caracteres.'
        end
      end


      group :social_network do
        active false
        label 'Redes Sociais'

        field :facebook do
          help 'Opcional. Ex.: http://www.facebook.com/nolan'
        end

        field :twitter do
          help 'Opcional. Ex.: http://www.twitter.com/nolan'
        end
      end

      group :footer do
        label 'Rodapé'
        active false
        field(:footer)
      end

      group :footer_other_languages do
        label 'Rodapé em outras línguas'
        active false

        field(:footer_en)
        field(:footer_fr)
        field(:footer_it)
        field(:footer_es)
        field(:footer_de)
        field(:footer_ru)
        field(:footer_eo)
        field(:footer_hi)
        field(:footer_tr)
        field(:footer_da)
        field(:footer_sv)
        field(:footer_zh)
        field(:footer_ja)
        field(:footer_ar)
        field(:footer_he)
      end

      field :site_url

      group :activated_translations do
        active false
        label 'Ativar/Desativar traduções'

        field :en_lang
        field :fr_lang
        field :it_lang
        field :es_lang
        field :de_lang
        field :ru_lang
        field :eo_lang
        field :hi_lang
        field :tr_lang
        field :da_lang
        field :sv_lang
        field :zh_lang
        field :ja_lang
        field :ar_lang
        field :he_lang
      end
    end
  end

  config.model Page do
    weight 2

    label "Página"
    label_plural "Páginas"
    object_label_method { :title }

    list do
      field :title
      field :published
    end

    edit do
      field :title
      field(:content) { ckeditor true }
      group :title_other_languages do
        label "Título e conteúdo em outras línguas"
        active false

        field :title_en
        field(:content_en) { ckeditor true }

        field :title_fr
        field(:content_fr) { ckeditor true }

        field :title_it
        field(:content_it) { ckeditor true }

        field :title_es
        field(:content_es) { ckeditor true }

        field :title_de
        field(:content_de) { ckeditor true }

        field :title_ru
        field(:content_ru) { ckeditor true }

        field :title_eo
        field(:content_eo) { ckeditor true }

        field :title_hi
        field(:content_hi) { ckeditor true }

        field :title_tr
        field(:content_tr) { ckeditor true }

        field :title_da
        field(:content_da) { ckeditor true }

        field :title_sv
        field(:content_sv) { ckeditor true }

        field :title_zh
        field(:content_zh) { ckeditor true }

        field :title_ja
        field(:content_ja) { ckeditor true }

        field :title_ar
        field(:content_ar) { ckeditor true }

        field :title_he
        field(:content_he) { ckeditor true }

      end
      field :published
    end
  end

  config.model Participant do
    weight 5

    label "Participante"
    label_plural "Participantes"
    object_label_method { :name }

    list do
      field :name
      field :email
      field :city_name
      field :country
      field :position_result do
        formatted_value do
          "#{Participant::POSITIONS.index(value)}"
        end
      end
      field :receive_email
    end

    show do
      field :name
      field :email
      field :city_name
      field :receive_email
      field :position_result do
        formatted_value do
          "#{Participant::POSITIONS.index(value)}"
        end
      end
      field :economic_freedom_score do
        formatted_value do
          "#{(value*10).to_s + '%'}"
        end
      end
      field :civil_liberty_score do
        formatted_value do
          "#{(value*10).to_s + '%'}"
        end
      end
      field :response_ids do
        label 'Respostas'
        formatted_value do
          string = '<ul>'
          value.each do |id|
            response = Response.find(id)
            string += "<li><em>#{response.question.question[0..-2]}?</em> #{response.pretty_pt}</li>"
          end
          string += '</ul>'
          string.html_safe
        end
      end
    end

    export do
      field :name
      field :email
      field :city_name do
        label 'Cidade'
      end
      field :country
      field :receive_email_pretty do
        label 'Gostaria de receber novidades?'
      end
      field :position_result do
        formatted_value do
          "#{Participant::POSITIONS.index(value)}"
        end
      end
      field :economic_freedom_score do
        formatted_value do
          "#{(value*10).to_s + '%'}"
        end
      end
      field :civil_liberty_score do
        formatted_value do
          "#{(value*10).to_s + '%'}"
        end
      end
      # field :response_ids do
      #   label 'Respostas'
      #   formatted_value do
      #     string = ''
      #     value.each do |id|
      #       response = Response.find(id)
      #       string += "#{response.question.question[0..-2]}? #{response.pretty_pt}\n"
      #     end
      #     string[0..-2]
      #   end
      # end
    end
  end

  config.model Question do
    weight 4
    label "Pergunta"
    label_plural "Perguntas"
    object_label_method { :question }
    list do
      field :question
      field :category
      field :weight
    end

    edit do
      field :question
      field :category, :enum
      field :weight

      group :question_other_languages do
        label "Pergunta em outras línguas"
        active false
        field :question_en
        field :question_fr
        field :question_it
        field :question_es
        field :question_de
        field :question_ru
        field :question_eo
        field :question_hi
        field :question_tr
        field :question_da
        field :question_sv
        field :question_zh
        field :question_ja
        field :question_ar
        field :question_he
      end
    end
  end

  config.model Sponsor do
    weight 3
    label "Patrocinador"
    label_plural "Patrocinadores"
    list do
      field :image
      field :name
      field :link
    end

    edit do
      field :name
      field :link do
        help 'Opcional. Ex.: http://www.exemplo.com.br'
      end
      field :image do
        thumb_method :small
      end
    end
  end

  config.model User do
    weight 6
    label "Usuário"
    label_plural "Usuários"
    object_label_method { :email }

    list do
      field :email
    end

    edit do
      field :email
      field(:password) do
        label 'Senha'
        help 'Obrigatório.'
      end
      field :password_confirmation do
        label 'Confirme a senha'
        help 'Repita a senha.'
      end
    end
  end

  config.model City do
    weight 7
    label "Cidades"
    label_plural "Cidades"
    object_label_method { :name }

    list do
      field :name
    end

    edit do
      field :name
    end
  end

  config.model Report do
    weight 9
    label 'Relatório'
    label 'Relatórios'

    list do
      field(:name) do
        label 'Nome'
      end
    end

    show do
      field(:all_reports) do
        label 'Relatório'
      end
    end
  end

  # CKeditor models (begin)
  config.model Ckeditor::Asset do
    visible false
  end

  config.model Ckeditor::Picture do
    label "Imagem"
    label_plural "Imagens"
    navigation_label 'Arquivos adicionados através do editor'
    weight 20

    edit do
     field(:data) { label 'Imagem' }
    end

    list do
     field(:data) { label 'Imagem' }
     field(:created_at) { label 'Criado em' }
     field(:updated_at) { label 'Atualizado em' }
    end
  end

  config.model Ckeditor::AttachmentFile do
    label 'Arquivo'
    navigation_label 'Arquivos adicionados através do editor'
    weight 20

    edit do
     field(:data) { label 'Arquivo' }
    end

    list do
     field(:data) do
        label 'Arquivo'
        pretty_value do # used in list view columns and show views, defaults to formatted_value for non-association fields
         "<a href='#{value.url}' target='_blank'>#{value.original_filename}</a>".html_safe
       end
     end

     field(:created_at) { label 'Criado em' }
     field(:updated_at) { label 'Atualizado em' }
    end
  end
  # CKeditor models (end)
end
