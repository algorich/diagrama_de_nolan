# -*- encoding : utf-8 -*-
require "spec_helper"

describe LectureRequestMailer do
  describe 'send lecture request' do
    before do
      @configuration = Factory.create :configuration
      @params = {:name => 'Fulano', :email => 'fulano@gmail.com',
                :phone => '2732-3915', :city => 'Campos dos Goytacazes',
                :state => 'RJ', :place => 'UENF', :to => @configuration.email}
    end

    let(:mail) { LectureRequestMailer.lecture_request(@params) }

    it 'guarantee that the receiver is correct' do
      mail.to.should == [@params[:to]]
    end

    it 'guarantee that the sender is correct' do
      mail.from.should == [@params[:email]]
    end

    it 'guarantee that the subject is correct' do
      mail.subject.should == "[Solicitação de palestra] #{@params[:name]}"
    end

    it 'guarantee that all information appears on the email body' do
      mail.body.encoded.should =~ /Nome: #{@params[:name]}/
      mail.body.encoded.should =~ /E-mail: #{@params[:email]}/
      mail.body.encoded.should =~ /Telefone: #{@params[:phone]}/
      mail.body.encoded.should =~ /Cidade: #{@params[:city]}/
      mail.body.encoded.should =~ /UF: #{@params[:state]}/
      mail.body.encoded.should =~ /Local:\r\n#{@params[:place]}/
    end
  end
end

