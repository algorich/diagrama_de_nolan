# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Configuration do
  context 'validations' do
    context 'email' do
      it { should have_valid(:facebook).when('http://google.com') }
      it { should_not have_valid(:facebook).when('foo') }
      it { should have_valid(:twitter).when('http://google.com') }
      it { should_not have_valid(:twitter).when('foo') }
      it { should_not have_valid(:email).when('') }
      it { should_not have_valid(:email).when(nil) }
      it { should_not have_valid(:email).when('contato@empresa') }
      it { should have_valid(:email).when('contato@empresa.com') }
    end
  end
end

