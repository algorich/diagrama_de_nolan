# -*- encoding : utf-8 -*-

require 'spec_helper'

describe Participant do
  def create_participant(civil ,economic)
	  city = Factory.create :city
	  Factory.create :participant, :civil_liberty_score => civil,
	      :economic_freedom_score => economic, :city => city
  end

  context 'validations' do
    it { should_not have_valid(:name).when('', nil) }
    it { should_not have_valid(:email).when('', nil, 'foo@bar') }
    it { should_not have_valid(:country).when('', nil) }
    it { should have_valid(:email).when('foo@bar.com') }

    it 'city' do
      participant = create_participant(10, 10)
      participant.country = 'Brazil'
      participant.city_id = nil
      participant.save
      participant.should_not be_valid

      participant.city_id = 2
      participant.save
      participant.should be_valid
    end
  end

  context 'set the participant position' do
  	it 'centrist' do
  		participant = create_participant(5,6)
  		participant.position_result.should == Participant::POSITIONS['Centrista']
  	end

  	it 'libertarian' do
  		participant = create_participant(8,8)
  		participant.position_result.should == Participant::POSITIONS['Libertário']
  	end

  	it 'left' do
  		participant = create_participant(6,2)
  		participant.position_result.should == Participant::POSITIONS['Esquerda']
  	end

  	it 'right' do
  		participant = create_participant(4,8)
  		participant.position_result.should == Participant::POSITIONS['Direita']
  	end

  	it 'statist' do
  		participant = create_participant(1,1)
  		participant.position_result.should == Participant::POSITIONS['Estatista']
  	end
  end

  it 'should set city name' do
    city = Factory.create :city, :name => 'Campos'
    participant = Factory.create :participant, :city => city
    participant.city_name.should == 'Campos'
  end

  context 'statistics' do
    before do
      @statistics = Participant.statistics
    end

    context 'when there is no participant' do
      it 'libertarian' do
        @statistics[:libertarian][:result].should == 0.0
        @statistics[:libertarian][:civil_liberty].should == 0.0
        @statistics[:libertarian][:economic_freedom].should == 0.0
      end

      it 'left' do
        @statistics[:left][:result].should == 0.0
        @statistics[:left][:civil_liberty].should == 0.0
        @statistics[:left][:economic_freedom].should == 0.0
      end

      it 'centrist' do
        @statistics[:centrist][:result].should == 0.0
        @statistics[:centrist][:civil_liberty].should == 0.0
        @statistics[:centrist][:economic_freedom].should == 0.0
      end

      it 'right' do
        @statistics[:right][:result].should == 0.0
        @statistics[:right][:civil_liberty].should == 0.0
        @statistics[:right][:economic_freedom].should == 0.0
      end

      it 'statist' do
        @statistics[:statist][:result].should == 0.0
        @statistics[:statist][:civil_liberty].should == 0.0
        @statistics[:statist][:economic_freedom].should == 0.0
      end

      it 'total_economic_freedom and total_civil_liberty' do
        @statistics[:total_economic_freedom].should == 0.0
        @statistics[:total_civil_liberty].should == 0.0
      end
    end

    context 'when participantes are present' do
      before do
        @city = Factory.create :city
        5.times do
        Factory.create :participant, :city => @city,
            :position_result => Participant::POSITIONS['Estatista'],
            :economic_freedom_score => 1,
            :civil_liberty_score => 1
        end
        5.times do
          Factory.create :participant, :city => @city,
              :position_result => Participant::POSITIONS['Direita'],
              :economic_freedom_score => 8,
              :civil_liberty_score => 4
        end
        3.times do
          Factory.create :participant, :city => @city,
              :position_result => Participant::POSITIONS['Esquerda'],
              :economic_freedom_score => 2,
              :civil_liberty_score => 6
        end
        2.times do
          Factory.create :participant, :city => @city,
              :position_result => Participant::POSITIONS['Libertário'],
              :economic_freedom_score => 8,
              :civil_liberty_score => 8
        end
        5.times do
          Factory.create :participant, :city => @city,
              :position_result => Participant::POSITIONS['Centrista'],
              :economic_freedom_score => 6,
              :civil_liberty_score => 5
        end
        @statistics = Participant.statistics
      end

      it 'libertarian' do
        @statistics[:libertarian][:result].should == 10.0
        @statistics[:libertarian][:civil_liberty].should == 80.0
        @statistics[:libertarian][:economic_freedom].should == 80.0
      end

      it 'left' do
        @statistics[:left][:result].should == 15.0
        @statistics[:left][:civil_liberty].should == 60.0
        @statistics[:left][:economic_freedom].should == 20.0
      end

      it 'centrist' do
        @statistics[:centrist][:result].should == 25.0
        @statistics[:centrist][:civil_liberty].should == 50.0
        @statistics[:centrist][:economic_freedom].should == 60.0
      end

      it 'right' do
        @statistics[:right][:result].should == 25.0
        @statistics[:right][:civil_liberty].should == 40.0
        @statistics[:right][:economic_freedom].should == 80.0
      end

      it 'statist' do
        @statistics[:statist][:result].should == 25.0
        @statistics[:statist][:civil_liberty].should == 10.0
        @statistics[:statist][:economic_freedom].should == 10.0
      end

      it 'total_civil_liberty and total_economic_freedom' do
        @statistics[:total_economic_freedom].should == 48.5
        @statistics[:total_civil_liberty].should == 42.0
      end
    end
  end
end
