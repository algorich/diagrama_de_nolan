# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Sponsor do
  context 'validations' do
    it { should have_valid(:link).when('http://google.com') }
    it { should_not have_valid(:link).when('foo') }
    it { should have_valid(:name).when('Foo bar') }
  end
end

