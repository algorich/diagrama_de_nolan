# -*- encoding : utf-8 -*-

require 'spec_helper'

describe Question do
  context 'validations' do
    it { should_not have_valid(:question).when('', nil) }
    it { should_not have_valid(:weight).when('', nil) }
    it { should_not have_valid(:category).when('', nil) }
  end
end
