# -*- encoding : utf-8 -*-
require "spec_helper"

describe SiteController do
  it 'index page' do
    { :get => '/' }.should route_to(:controller => 'site', :action => 'index')
  end

  it 'lecture request page' do
    { :get => '/solicitar-palestra' }.should route_to(:controller => 'site', :action => 'lecture_request')
  end

  it 'send lecture request' do
    { :post => '/solicitar-palestra' }.should route_to(:controller => 'site', :action => 'send_lecture_request')
  end

  it 'pages' do
    { :get => '/faca-uma-doacao' }.should route_to(:controller => 'site', :action => 'page', :page => 'faca-uma-doacao')
  end
end

