FactoryGirl.define do
	factory :ckeditor_picture, :class => Ckeditor::Picture do
		data File.new("#{Rails.root}/spec/data/image.jpg")
	end
end