# -*- encoding : utf-8 -*-

FactoryGirl.define do
  factory :page do
    title "MyString"
    content "MyText"
  end
end
