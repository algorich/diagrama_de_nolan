# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :question do
    question "MyText"
    weight 5
    category Question::CATEGORY[:economic_freedom]

    question_en "MyText"
    question_fr "MyText"
    question_it "MyText"
    question_es "MyText"
    question_de "MyText"
    question_eo "MyText"
    question_hi "MyText"
    question_tr "MyText"
    question_da "MyText"
    question_sv "MyText"
    question_zh "MyText"
    question_ja "MyText"
    question_ar "MyText"
    question_he "MyText"
  end
end
