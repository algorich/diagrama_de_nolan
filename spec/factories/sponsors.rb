# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :sponsor do
    name 'MyString'
    image_file_name 'spec/acceptance/data/image.jpg'
    link 'http://google.com'
  end
end

