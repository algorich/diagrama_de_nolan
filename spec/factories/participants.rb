# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :participant do
    name "MyString"
    email "some@email.com"
    country "Brazil"
  end
end
