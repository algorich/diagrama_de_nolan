# -*- encoding : utf-8 -*-
require 'spec_helper'

def choose_aswers answers
  answers[0..4].each_with_index do |a, i|
    i+=1
    within("#question_#{i}") do
      find(a).click
    end
  end
  click_link 'Questões econômicas'
  answers[5..9].each_with_index do |a, i|
    i+=6
    within("#question_#{i}") do
      find(a).click
    end
  end
end

feature 'Answer all the questions' do
  background do
    @user = Factory.create :user, :email => 'user@user.com', :password => '123456'
    Factory.create :configuration

    # Liberdade civil
    @question1 = Factory.create :question, :category => Question::CATEGORY[:civil_liberties], :question => 'A', :weight => 10
    @question2 = Factory.create :question, :category => Question::CATEGORY[:civil_liberties], :question => 'B', :weight => 9
    @question3 = Factory.create :question, :category => Question::CATEGORY[:civil_liberties], :question => 'C', :weight => 8
    @question4 = Factory.create :question, :category => Question::CATEGORY[:civil_liberties], :question => 'D', :weight => 7
    @question5 = Factory.create :question, :category => Question::CATEGORY[:civil_liberties], :question => 'E', :weight => 6
    @extra_question1 = Factory.create :question, :category => Question::CATEGORY[:civil_liberties], :question => 'Extra 1', :weight => 5

    # Liberdade econômica
    @question6 = Factory.create :question, :category => Question::CATEGORY[:economic_freedom], :question => 'F', :weight => 4
    @question7 = Factory.create :question, :category => Question::CATEGORY[:economic_freedom], :question => 'G', :weight => 3
    @question8 = Factory.create :question, :category => Question::CATEGORY[:economic_freedom], :question => 'H', :weight => 2
    @question9 = Factory.create :question, :category => Question::CATEGORY[:economic_freedom], :question => 'I', :weight => 1
    @question10 = Factory.create :question, :category => Question::CATEGORY[:economic_freedom], :question => 'J', :weight => 0
    @extra_question2 = Factory.create :question, :category => Question::CATEGORY[:economic_freedom], :question => 'Extra 2', :weight => -1

    @city = Factory.create :city, :name => "Derps City"
  end

  scenario 'success', :js => true do
    visit questionnaire_path

    # Liberdade civil
    within("#question_1") do
      page.should have_content 'A'
      find('.agree').click
    end
    within("#question_2") do
      page.should have_content 'B'
      find('.disagree').click
    end
    within("#question_3") do
      page.should have_content 'C'
      find('.perhaps').click
    end
    within("#question_4") do
      page.should have_content 'D'
      find('.agree').click
    end
    within("#question_5") do
      page.should have_content 'E'
      find('.disagree').click
    end
    page.should_not have_content 'EXTRA 1'

    click_link 'Questões econômicas'
    Response.all.should be_empty

    # Liberdade econômica
    within("#question_6") do
      page.should have_content 'F'
      find('.agree').click
    end
    within("#question_7") do
      page.should have_content 'G'
      find('.disagree').click
    end
    within("#question_8") do
      page.should have_content 'H'
      find('.perhaps').click
    end
    within("#question_9") do
      page.should have_content 'I'
      find('.agree').click
    end
    within("#question_10") do
      page.should have_content 'J'
      find('.disagree').click
    end
    page.should_not have_content 'EXTRA 2'

    click_link 'Dados pessoais'

    # Participant info
    fill_in 'Nome', :with => 'Derp'
    fill_in 'E-mail', :with => 'derp@email.com'
    select 'Brazil', :from => 'País'
    fill_in_token_input 'token-input-questionnaire_participant_city_token', :with => 'De'
    click_button 'Obter resultado'
    sleep 1

    answered_questions = Response.all.map(&:question_id)
    answered_questions.should == [
      @question1.id, @question2.id, @question3.id,
      @question4.id, @question5.id, @question6.id,
      @question7.id, @question8.id, @question9.id, @question10.id
    ]
    answered_questions.should_not == [@extra_question1.id, @extra_question2.id]

    Response.all.map(&:participant_id).each { |id| id.should == Participant.first.id }
  end

  scenario 'back to previous questions', :js => true do
    visit questionnaire_path

    # Liberdade civil
    within("#question_1") do
      page.should have_content 'A'
      find('.agree').click
    end
    within("#question_2") do
      page.should have_content 'B'
      find('.disagree').click
    end
    within("#question_3") do
      page.should have_content 'C'
      find('.perhaps').click
    end
    within("#question_4") do
      page.should have_content 'D'
      find('.agree').click
    end
    within("#question_5") do
      page.should have_content 'E'
      find('.disagree').click
    end
    page.should_not have_content 'EXTRA 1'

    click_link 'Questões econômicas'

    # Liberdade econômica
    within("#question_6") do
      page.should have_content 'F'
      find('.agree').click
    end
    within("#question_7") do
      page.should have_content 'G'
      find('.disagree').click
    end
    within("#question_8") do
      page.should have_content 'H'
      find('.agree').click
    end
    within("#question_9") do
      page.should have_content 'I'
      find('.agree').click
    end
    within("#question_10") do
      page.should have_content 'J'
      find('.disagree').click
    end
    page.should_not have_content 'EXTRA 2'

    click_link 'Dados pessoais'

    # Participant info
    fill_in 'Nome', :with => 'Derp'
    fill_in 'E-mail', :with => 'derp@email.com'
    select 'Brazil', :from => 'País'
    fill_in_token_input 'token-input-questionnaire_participant_city_token', :with => 'Derps'

    click_link 'Questões econômicas'
    click_link 'Questões civis'
    click_link 'Questões econômicas'
    click_link 'Dados pessoais'
    click_button 'Obter resultado'
    sleep 2

    @question1.responses.last.response.should == Response::ANSWERS[:agree]
    @question10.responses.last.response.should == Response::ANSWERS[:disagree]

    answered_questions = Response.all.map(&:question_id)
    answered_questions.sort.should == [
      @question1.id, @question2.id, @question3.id,
      @question4.id, @question5.id, @question6.id,
      @question7.id, @question8.id, @question9.id,
      @question10.id
    ]
    answered_questions.should_not == [@extra_question1.id, @extra_question2.id]

    Response.all.map(&:participant_id).each { |id| id.should == Participant.first.id }
  end

  context 'failure' do
    scenario 'invalid/blank participant info', :js => true do
      visit questionnaire_path

      # Liberdade civil
      within("#question_1") do
        page.should have_content 'A'
        find('.agree').click
      end
      within("#question_2") do
        page.should have_content 'B'
        find('.disagree').click
      end
      within("#question_3") do
        page.should have_content 'C'
        find('.perhaps').click
      end
      within("#question_4") do
        page.should have_content 'D'
        find('.agree').click
      end
      within("#question_5") do
        page.should have_content 'E'
        find('.agree').click
      end

      click_link 'Questões econômicas'

      # Liberdade econômica
      within("#question_6") do
        page.should have_content 'F'
        find('.agree').click
      end
      within("#question_7") do
        page.should have_content 'G'
        find('.disagree').click
      end
      within("#question_8") do
        page.should have_content 'H'
        find('.perhaps').click
      end
      within("#question_9") do
        page.should have_content 'I'
        find('.agree').click
      end
      within("#question_10") do
        page.should have_content 'J'
        find('.disagree').click
      end

      click_link 'Dados pessoais'

      fill_in 'Nome', :with => 'Nome'
      fill_in 'E-mail', :with => 'derp@mail'
      select 'Brazil', :from => 'País'
      fill_in_token_input 'token-input-questionnaire_participant_city_token', :with => 'Derp'
      uncheck 'Gostaria de receber novidades?'

      click_button 'Obter resultado'

      Response.all.should be_empty
      Participant.all.should be_empty

      page.should have_content 'não é válido.'
    end
  end

  context 'verify result calculated' do
    scenario 'libertarian', :js => true do
      visit questionnaire_path
      answers = []
      10.times do
        answers << '.disagree'
      end
      choose_aswers answers

      click_link 'Dados pessoais'

      # Participant info
      fill_in 'Nome', :with => 'Derp'
      fill_in 'E-mail', :with => 'derp@email.com'
      select 'Brazil', :from => 'País'
      fill_in_token_input 'token-input-questionnaire_participant_city_token', :with => 'De'
      click_button 'Obter resultado'
      sleep 3

      participant = Participant.last
      participant.position_result.should == Participant::POSITIONS['Libertário']
      participant.economic_freedom_score.should == 10
      participant.civil_liberty_score.should == 10
    end

    scenario 'statist', :js => true do
      visit questionnaire_path
      answers = []
      10.times do
        answers << '.agree'
      end
      choose_aswers answers

      click_link 'Dados pessoais'

      # Participant info
      fill_in 'Nome', :with => 'Derp'
      fill_in 'E-mail', :with => 'derp@email.com'
      select 'Brazil', :from => 'País'
      fill_in_token_input 'token-input-questionnaire_participant_city_token', :with => 'De'
      click_button 'Obter resultado'
      sleep 1

      participant = Participant.last
      participant.position_result.should == Participant::POSITIONS['Estatista']
      participant.economic_freedom_score.should == 0
      participant.civil_liberty_score.should == 0
    end

    scenario 'centrist', :js => true do
      visit questionnaire_path
      answers = []
      10.times do
        answers << '.perhaps'
      end
      choose_aswers answers

      click_link 'Dados pessoais'

      # Participant info
      fill_in 'Nome', :with => 'Derp'
      fill_in 'E-mail', :with => 'derp@email.com'
      select 'Brazil', :from => 'País'
      fill_in_token_input 'token-input-questionnaire_participant_city_token', :with => 'De'
      click_button 'Obter resultado'
      sleep 1

      participant = Participant.last
      participant.position_result.should == Participant::POSITIONS['Centrista']
      participant.economic_freedom_score.should == 5
      participant.civil_liberty_score.should == 5
    end

    scenario 'left', :js => true do
      visit questionnaire_path
      answers = []
      # Liberdade civil
      5.times do
        answers << '.disagree'
      end
      # Liberdade economica
      5.times do
        answers << '.agree'
      end
      choose_aswers answers

      click_link 'Dados pessoais'

      # Participant info
      fill_in 'Nome', :with => 'Derp'
      fill_in 'E-mail', :with => 'derp@email.com'
      select 'Brazil', :from => 'País'
      fill_in_token_input 'token-input-questionnaire_participant_city_token', :with => 'De'
      click_button 'Obter resultado'
      sleep 1

      participant = Participant.last
      participant.position_result.should == Participant::POSITIONS['Esquerda']
      participant.economic_freedom_score.should == 0
      participant.civil_liberty_score.should == 10
    end

    scenario 'right', :js => true do
      visit questionnaire_path
      answers = []
      # Liberdade civil
      5.times do
        answers << '.agree'
      end
      # Liberdade economica
      5.times do
        answers << '.disagree'
      end
      choose_aswers answers

      click_link 'Dados pessoais'

      # Participant info
      fill_in 'Nome', :with => 'Derp'
      fill_in 'E-mail', :with => 'derp@email.com'
      select 'Brazil', :from => 'País'
      fill_in_token_input 'token-input-questionnaire_participant_city_token', :with => 'De'
      click_button 'Obter resultado'
      sleep 1

      participant = Participant.last
      participant.position_result.should == Participant::POSITIONS['Direita']
      participant.economic_freedom_score.should == 10
      participant.civil_liberty_score.should == 0
    end
  end
end

feature 'show option for non-brazilians' do
  class ApplicationController < ActionController::Base
    before_filter :mock_ip_address

    def mock_ip_address
      request.instance_eval <<-EOS
        def ip
          "189.25.3.76"
        end
      EOS
    end
  end

  background do
    @user = Factory.create :user, :email => 'user@user.com', :password => '123456'
    Factory.create :configuration

    # Liberdade civil
    @question1 = Factory.create :question, :category => Question::CATEGORY[:civil_liberties], :question => 'A', :weight => 10
    @question2 = Factory.create :question, :category => Question::CATEGORY[:civil_liberties], :question => 'B', :weight => 9
    @question3 = Factory.create :question, :category => Question::CATEGORY[:civil_liberties], :question => 'C', :weight => 8
    @question4 = Factory.create :question, :category => Question::CATEGORY[:civil_liberties], :question => 'D', :weight => 7
    @question5 = Factory.create :question, :category => Question::CATEGORY[:civil_liberties], :question => 'E', :weight => 6
    @extra_question1 = Factory.create :question, :category => Question::CATEGORY[:civil_liberties], :question => 'Extra 1', :weight => 5

    # Liberdade econômica
    @question6 = Factory.create :question, :category => Question::CATEGORY[:economic_freedom], :question => 'F', :weight => 4
    @question7 = Factory.create :question, :category => Question::CATEGORY[:economic_freedom], :question => 'G', :weight => 3
    @question8 = Factory.create :question, :category => Question::CATEGORY[:economic_freedom], :question => 'H', :weight => 2
    @question9 = Factory.create :question, :category => Question::CATEGORY[:economic_freedom], :question => 'I', :weight => 1
    @question10 = Factory.create :question, :category => Question::CATEGORY[:economic_freedom], :question => 'J', :weight => 0
    @extra_question2 = Factory.create :question, :category => Question::CATEGORY[:economic_freedom], :question => 'Extra 2', :weight => -1
  end

  scenario 'show option for non-brazilians', :js => true do
    visit questionnaire_path
    answers = []
    10.times do
      answers << '.perhaps'
    end
    choose_aswers answers

    click_link 'Dados pessoais'

    # Participant info
    check "I'm not Brazilian"
    find('div.input.country.required.country').should be_true
    find('div.input.string.required.city.hidden').should be_true
  end
end
