# -*- encoding : utf-8 -*-
require 'spec_helper'

feature "should render participant's report" do
	background do
		Factory.create :configuration
		@city = Factory.create :city
		5.times do
      Factory.create :participant, :city => @city, 
          :position_result => Participant::POSITIONS['Estatista'],
          :economic_freedom_score => 1,
          :civil_liberty_score => 1
      end
      5.times do
        Factory.create :participant, :city => @city, 
            :position_result => Participant::POSITIONS['Direita'],
            :economic_freedom_score => 8,
            :civil_liberty_score => 4
      end
      3.times do
        Factory.create :participant, :city => @city, 
            :position_result => Participant::POSITIONS['Esquerda'],
            :economic_freedom_score => 2,
            :civil_liberty_score => 6
      end
      2.times do
        Factory.create :participant, :city => @city, 
            :position_result => Participant::POSITIONS['Libertário'],
            :economic_freedom_score => 8,
            :civil_liberty_score => 8
      end
      5.times do
        Factory.create :participant, :city => @city, 
            :position_result => Participant::POSITIONS['Centrista'],
            :economic_freedom_score => 6,
            :civil_liberty_score => 5
      end
		report = Factory.create :report, :name => 'Porcentagem de posições políticas'
		@user = Factory.create :user
		login @user.email, '123456'
		visit "/admin/report/#{report.id}"
	end

	it 'should render the report' do
		page.should have_content 'Libertário: 10.0%'
		within('#libertarian_report') do
			page.should have_content 'Liberdade civil: 80.0%'
			page.should have_content 'Liberdade econômica: 80.0%'
		end

		page.should have_content 'Esquerda: 15.0%'
		within('#left_report') do
			page.should have_content 'Liberdade civil: 60.0%'
			page.should have_content 'Liberdade econômica: 20.0%'
		end

		page.should have_content 'Centro: 25.0%'
		within('#centrist_report') do
			page.should have_content 'Liberdade civil: 50.0%'
			page.should have_content 'Liberdade econômica: 60.0%'
		end

		page.should have_content 'Direita: 25.0%'
		within('#right_report') do
			page.should have_content 'Liberdade civil: 40.0%'
			page.should have_content 'Liberdade econômica: 80.0%'
		end

		page.should have_content 'Estatista: 25.0%'
		within('#statist_report') do
			page.should have_content 'Liberdade civil: 10.0%'
			page.should have_content 'Liberdade econômica: 10.0%'
		end

		within('#liberty') do
			page.should have_content 'Liberdade civil: 42.0%'
			page.should have_content 'Liberdade econômica: 48.5%'
		end
	end
end