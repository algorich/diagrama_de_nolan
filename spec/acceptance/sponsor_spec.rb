# -*- encoding : utf-8 -*-

require 'spec_helper'

feature 'Manupulate sponsors' do

  background do
    @user = Factory.create :user, :email => 'user@user.com', :password => '123456'
    Factory.create :configuration
    login(@user.email,'123456')
  end

  context 'new' do
    background do
      visit "/admin/sponsor/new"
    end

    scenario 'successfully', :js => true do
      previous_lenght = Sponsor.all.length
      fill_in 'Nome', :with => 'Foo'
      fill_in 'Site', :with => 'http://google.com'
      attach_file 'Logotipo', IMAGE
      click_button 'Salvar'
      page.should have_content 'Patrocinador criado(a) com sucesso.'
      Sponsor.all.length.should == previous_lenght + 1
    end

    context 'unsuccessfully', :js => true do
      scenario 'empty fields' do
        fill_in 'Nome', :with => ''
        click_button 'Salvar'
        page.should have_content 'Nome não pode ser vazio.'
      end

      scenario 'invalid logo format', :js => true do
        attach_file 'Logotipo', TEXT
        click_button 'Salvar'
        sleep 1
        page.should have_content 'Logotipo com formato inválido'
      end

      scenario 'invalid site format' do
        fill_in 'Site', :with => 'foo bar'
        click_button 'Salvar'
        page.should have_content 'Site não é válido'
      end
    end
  end

  context 'delete' do
    background do
      @sponsor = Factory.create :sponsor
      visit "/admin/sponsor"
    end

    scenario 'successfully', :js => true do
      previous_lenght = Sponsor.all.length
      click_link 'Excluir'
      click_button 'Sim, eu tenho certeza'
      page.should have_content 'Patrocinador excluído(a) com sucesso.'
      Sponsor.all.length.should == previous_lenght - 1
    end
  end
end

