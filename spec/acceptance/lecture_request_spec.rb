# -*- encoding : utf-8 -*-
require 'spec_helper'

feature 'Send a contact message' do
  background do
    Factory.create :configuration
    Factory.create :page, :indicator => Page::PAGES[:lecture], :published => true, :content => 'My content here.'
  end

  scenario 'successfully' do
    visit lecture_request_path
    fill_in 'Nome', :with => 'Fulano'
    fill_in 'E-mail', :with => 'fulano@gmail.com'
    fill_in 'Telefone', :with => '(22) 2732-3915'
    fill_in 'Cidade', :with => 'Campos dos Goytacazes'
    select 'RJ', :from => 'UF'
    fill_in 'Local onde será realizada a palestra', :with => 'UENF'
    click_button 'Solicitar palestra'
    sleep 2
    page.should have_content 'Solicitação de palestra enviada com sucesso.'
  end

  scenario 'failure' do
    visit lecture_request_path
    fill_in 'E-mail', :with => 'fulano@gmail'
    click_button 'Solicitar palestra'
    page.should have_content 'não pode ser vazio.'
    page.should have_content 'formato inválido.'
  end
end
