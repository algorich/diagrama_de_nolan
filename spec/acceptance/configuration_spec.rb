# -*- encoding : utf-8 -*-
require 'spec_helper'

def open_fieldsets n
  fieldsets = []
  n.times do |i|
    i+=1
    fieldsets << find(:xpath, "(//i[@class='icon-chevron-right'])[#{i}]")
  end
  fieldsets.map &:click
  sleep 2
end 

feature 'Manupulate configurations' do
  background do
    @user = Factory.create :user, :email => 'user@user.com', :password => '123456'
    @configuration = Factory.create :configuration
    login(@user.email,'123456')
    visit "/admin/configuration/#{@configuration.id}/edit"
  end

  context 'edit', :js => true do
    scenario 'successfully' do
      open_fieldsets(6)
      fill_in 'E-mail', :with => 'email@qualquer.com'
      fill_in 'Palavras-chave', :with => 'project, base'
      fill_in 'Descrição', :with => 'This is the project base'
      fill_in 'Twitter', :with => 'http://twitter.com/foo'
      fill_in 'Facebook', :with => 'http://facebook.com/foo'
      fill_in 'Endereço do site', :with => 'http://facebook.com/foo'
      fill_in 'Google Analytics', :with => 'foo'
      fill_in 'Google Analytics Page Tracker', :with => 'foo'
      fill_in 'Rodapé', :with => 'Rodapé qualquer'
      check "Inglês"
      check "Francês"
      check "Italiano"
      check "Espanhol"
      check "Alemão"
      check "Esperanto"
      check "Hindi"
      check "Turco"
      check "Dinamarquês"
      check "Sueco"
      check "Chinês"
      check "Japonês"
      check "Árabe"
      check "Hebraico"
      click_button 'Salvar'
      page.should have_content 'Configuração atualizado(a) com sucesso.'

      visit '/'
      page.should have_xpath('.//meta[@name="keywords"]', :content => 'project, base')
      page.should have_xpath('.//meta[@name="description"]', :content => 'This is the project base')
    end

    context 'unsuccessfully' do
      scenario 'invalid email' do
        open_fieldsets(6)
        fill_in 'E-mail', :with => 'email@qualquer'
        click_button 'Salvar'
        page.should have_content 'E-mail não é válido.'
      end

      scenario 'empty fields' do
        open_fieldsets(6)
        fill_in 'E-mail', :with => ''
        fill_in 'Endereço do site', :with => ''
        click_button 'Salvar'
        page.should have_content 'E-mail não pode ser vazio.'
        page.should have_content 'Endereço do site não pode ser vazio.'
      end

      scenario 'invalid url format' do
        open_fieldsets(6)
        fill_in 'Facebook', :with => 'foo bar'
        fill_in 'Twitter', :with => 'foo bar'
        fill_in 'Endereço do site', :with => 'foo bar'
        click_button 'Salvar'
        page.should have_content 'Facebook não é válido'
        page.should have_content 'Twitter não é válido'
        page.should have_content 'Endereço do site não é válido'
      end
    end
  end

  scenario 'cannot add a new' do
    lambda { visit '/admin/configuration/new' }.should raise_error CanCan::AccessDenied
  end

  scenario 'cannot delete' do
    lambda {
      visit "/admin/configuration/#{@configuration.id}/delete"
    }.should raise_error CanCan::AccessDenied
  end
end

