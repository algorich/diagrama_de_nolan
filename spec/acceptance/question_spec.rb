# -*- encoding : utf-8 -*-
require 'spec_helper'

feature 'Manipulate question' do
  background do
    @user = Factory.create :user, :email => 'user@user.com', :password => '123456'
    Factory.create :configuration
    login(@user.email,'123456')
  end

  context 'new' do
    background do
      visit '/admin/question/new'
    end

    scenario 'successfully' do
      previous_lenght = Question.all.length

      fill_in 'Pergunta', :with => 'Derp?'
      select 'Liberdade econômica', :from => 'Categoria'
      fill_in 'Peso', :with => '5'

      fill_in 'Pergunta em inglês', :with => 'Foo?'
      fill_in 'Pergunta em francês', :with => 'Foo?'
      fill_in 'Pergunta em italiano', :with => 'Foo?'
      fill_in 'Pergunta em espanhol', :with => 'Foo?'
      fill_in 'Pergunta em alemão', :with => 'Foo?'
      fill_in 'Pergunta em esperanto', :with => 'Foo?'
      fill_in 'Pergunta em hindi', :with => 'Foo?'
      fill_in 'Pergunta em turco', :with => 'Foo?'
      fill_in 'Pergunta em dinamarquês', :with => 'Foo?'
      fill_in 'Pergunta em sueco', :with => 'Foo?'
      fill_in 'Pergunta em chinês', :with => 'Foo?'
      fill_in 'Pergunta em japonês', :with => 'Foo?'
      fill_in 'Pergunta em árabe', :with => 'Foo?'
      fill_in 'Pergunta em hebraico', :with => 'Foo?'

      click_button 'Salvar'
      page.should have_content 'Pergunta criado(a) com sucesso.'
      Question.all.length.should == previous_lenght + 1
    end

    context 'unsuccessfully' do
      scenario 'empty fields' do
        previous_lenght = Question.all.length
        fill_in 'Pergunta', :with => ''
        select '', :from => 'Categoria'
        fill_in 'Peso', :with => ''
        click_button 'Salvar'
        page.should have_content 'Pergunta não pode ser vazio.'
        page.should have_content 'Categoria não pode ser vazio.'
        page.should have_content 'Peso não pode ser vazio.'
        Question.all.length.should == previous_lenght
      end
    end
  end

  context 'edit' do
    before :each do
      question = Factory.create :question
      visit "/admin/question/#{question.id}/edit"
    end

    scenario 'successfully' do
      fill_in 'Pergunta', :with => 'Foo?'
      select 'Liberdade civil', :from => 'Categoria'
      fill_in 'Peso', :with => '5'
      click_button 'Salvar'
      page.should have_content 'Pergunta atualizado(a) com sucesso.'
    end

    scenario 'empty fields' do
      fill_in 'Pergunta', :with => ''
      select '', :from => 'Categoria'
      fill_in 'Peso', :with => ''
      click_button 'Salvar'
      page.should have_content 'Pergunta não pode ser vazio.'
      page.should have_content 'Categoria não pode ser vazio.'
      page.should have_content 'Peso não pode ser vazio.'
    end
  end

  context 'delete' do
    background do
      @question = Factory.create :question
      visit '/admin/question'
    end

    scenario 'successfully' do
      previous_lenght = Question.all.length

      click_link 'Excluir'
      click_button 'Sim, eu tenho certeza'
      page.should have_content 'Pergunta excluído(a) com sucesso.'
      Question.all.length.should == previous_lenght - 1
    end
  end
end

