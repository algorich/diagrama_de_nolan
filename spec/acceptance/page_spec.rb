# -*- encoding : utf-8 -*-
require 'spec_helper'

feature 'Manipulate page' do
  background do
    @user = Factory.create :user, :email => 'user@user.com', :password => '123456'
    Factory.create :configuration
    login(@user.email,'123456')
  end

  scenario 'new should rais CanCan::AccessDenied' do
    expect { visit '/admin/page/new' }.should raise_error(CanCan::AccessDenied)
  end

  context 'edit' do
    before :each do
      page = Factory.create :page
      visit "/admin/page/#{page.id}/edit"
    end

    scenario 'successfully' do
      fill_in 'Título', :with => 'Novo título'
      fill_in 'Conteúdo', :with => ''

      fill_in 'Título em inglês', :with => 'New title'
      fill_in 'Conteúdo em inglês', :with => ''

      fill_in 'Título em francês', :with => 'nouveau titre'
      fill_in 'Conteúdo em francês', :with => ''

      fill_in 'Título em italiano', :with => 'nuovo titolo'
      fill_in 'Conteúdo em italiano', :with => ''

      fill_in 'Título em espanhol', :with => 'nuevo título'
      fill_in 'Conteúdo em espanhol', :with => ''

      fill_in 'Título em alemão', :with => 'neuen Titel'
      fill_in 'Conteúdo em alemão', :with => ''

      fill_in 'Título em esperanto', :with => 'nova titolo'
      fill_in 'Conteúdo em esperanto', :with => ''

      fill_in 'Título em hindi', :with => 'नया शीर्षक'
      fill_in 'Conteúdo em hindi', :with => ''

      fill_in 'Título em turco', :with => 'yeni başlık'
      fill_in 'Conteúdo em turco', :with => ''

      fill_in 'Título em dinamarquês', :with => 'ny titel'
      fill_in 'Conteúdo em dinamarquês', :with => ''

      fill_in 'Título em sueco', :with => 'ny avdelning'
      fill_in 'Conteúdo em sueco', :with => ''

      fill_in 'Título em chinês', :with => '新標題'
      fill_in 'Conteúdo em chinês', :with => ''

      fill_in 'Título em japonês', :with => '新しいタイトル'
      fill_in 'Conteúdo em japonês', :with => ''

      fill_in 'Título em árabe', :with => 'لقب جديد'
      fill_in 'Conteúdo em árabe', :with => ''

      fill_in 'Título em hebraico', :with => 'כותרת חדשה'
      fill_in 'Conteúdo em hebraico', :with => ''
      check 'Publicado'
      click_button 'Salvar'
      page.should have_content 'Página atualizado(a) com sucesso.'
    end

    scenario 'failure' do
      fill_in 'Título', :with => ''
      click_button 'Salvar'
      page.should have_content 'Título não pode ser vazio.'
    end
  end
end

