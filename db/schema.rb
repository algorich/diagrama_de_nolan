# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130902134925) do

  create_table "cities", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "ckeditor_assets", :force => true do |t|
    t.string   "data_file_name",                  :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 30
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], :name => "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_ckeditor_assetable_type"

  create_table "configurations", :force => true do |t|
    t.string   "email"
    t.string   "keywords"
    t.text     "description"
    t.string   "facebook"
    t.string   "twitter"
    t.string   "site_url"
    t.text     "footer"
    t.text     "google_analytics"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.text     "footer_en"
    t.text     "footer_fr"
    t.text     "footer_it"
    t.text     "footer_es"
    t.text     "footer_de"
    t.text     "footer_ru"
    t.text     "footer_eo"
    t.text     "footer_hi"
    t.text     "footer_tr"
    t.text     "footer_da"
    t.text     "footer_sv"
    t.text     "footer_zh"
    t.text     "footer_ja"
    t.text     "footer_ar"
    t.text     "footer_he"
    t.text     "google_analytics_page_tracker"
    t.boolean  "en_lang"
    t.boolean  "fr_lang"
    t.boolean  "it_lang"
    t.boolean  "es_lang"
    t.boolean  "de_lang"
    t.boolean  "ru_lang"
    t.boolean  "eo_lang"
    t.boolean  "hi_lang"
    t.boolean  "tr_lang"
    t.boolean  "da_lang"
    t.boolean  "sv_lang"
    t.boolean  "zh_lang"
    t.boolean  "ja_lang"
    t.boolean  "ar_lang"
    t.boolean  "he_lang"
  end

  create_table "pages", :force => true do |t|
    t.string   "title"
    t.text     "content"
    t.boolean  "published",  :default => true
    t.integer  "indicator"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.string   "title_en"
    t.text     "content_en"
    t.string   "title_fr"
    t.text     "content_fr"
    t.string   "title_it"
    t.text     "content_it"
    t.string   "title_es"
    t.text     "content_es"
    t.string   "title_de"
    t.text     "content_de"
    t.string   "title_ru"
    t.text     "content_ru"
    t.string   "title_eo"
    t.text     "content_eo"
    t.string   "title_hi"
    t.text     "content_hi"
    t.string   "title_tr"
    t.text     "content_tr"
    t.string   "title_da"
    t.text     "content_da"
    t.string   "title_sv"
    t.text     "content_sv"
    t.string   "title_zh"
    t.text     "content_zh"
    t.string   "title_ja"
    t.text     "content_ja"
    t.string   "title_ar"
    t.text     "content_ar"
    t.string   "title_he"
    t.text     "content_he"
  end

  create_table "participants", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.boolean  "receive_email",          :default => true
    t.integer  "city_id"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.string   "country",                :default => "Brazil"
    t.integer  "position_result"
    t.integer  "economic_freedom_score"
    t.integer  "civil_liberty_score"
    t.string   "city_name"
  end

  create_table "questions", :force => true do |t|
    t.text     "question"
    t.integer  "category"
    t.integer  "weight"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.text     "question_en"
    t.text     "question_fr"
    t.text     "question_it"
    t.text     "question_es"
    t.text     "question_de"
    t.text     "question_ru"
    t.text     "question_eo"
    t.text     "question_hi"
    t.text     "question_tr"
    t.text     "question_da"
    t.text     "question_sv"
    t.text     "question_zh"
    t.text     "question_ja"
    t.text     "question_ar"
    t.text     "question_he"
  end

  create_table "rails_admin_histories", :force => true do |t|
    t.text     "message"
    t.string   "username"
    t.integer  "item"
    t.string   "table"
    t.integer  "month",      :limit => 2
    t.integer  "year",       :limit => 8
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "rails_admin_histories", ["item", "table", "month", "year"], :name => "index_rails_admin_histories"

  create_table "reports", :force => true do |t|
    t.string "name"
  end

  create_table "responses", :force => true do |t|
    t.integer  "participant_id"
    t.integer  "question_id"
    t.integer  "response"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "sponsors", :force => true do |t|
    t.string   "name"
    t.string   "link"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
