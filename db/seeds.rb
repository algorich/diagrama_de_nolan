# -*- encoding : utf-8 -*-

def file_to_string file_name
  lines = File.open(file_name).collect { |line| line }

  return lines.empty? ? '' : lines * ''
end

# ==============================================================================

User.delete_all

User.create!(
  :email => 'admin@base.com',
  :password => '123456'
)

Configuration.delete_all

Configuration.create!(
  :email => 'contato@estudantespelaliberdade.com.br',
  :twitter => 'http://twitter.com/epliberdade',
  :facebook => 'http://www.facebook.com/diagramadenolan',
  :site_url => 'http://nolan.estudantespelaliberdade.com.br',
  :google_analytics => file_to_string('public/seeds/analytics.part.html'),
  :google_analytics_page_tracker => file_to_string('public/seeds/analytics_page_tracker.part.html'),

  :footer    => file_to_string('public/seeds/pt/footer.part.html'),
  :footer_en => file_to_string('public/seeds/en/footer.part.html'),
  :footer_fr => file_to_string('public/seeds/fr/footer.part.html'),
  :footer_it => file_to_string('public/seeds/it/footer.part.html'),
  :footer_es => file_to_string('public/seeds/es/footer.part.html'),
  :footer_de => file_to_string('public/seeds/ge/footer.part.html'),
  :footer_ru => file_to_string('public/seeds/ru/footer.part.html'),
  :footer_eo => file_to_string('public/seeds/esp/footer.part.html'),
  :footer_hi => file_to_string('public/seeds/hi/footer.part.html'),
  :footer_tr => file_to_string('public/seeds/tu/footer.part.html'),
  :footer_da => file_to_string('public/seeds/din/footer.part.html'),
  :footer_sv => file_to_string('public/seeds/sue/footer.part.html'),
  :footer_zh => file_to_string('public/seeds/ch/footer.part.html'),
  :footer_ja => file_to_string('public/seeds/jap/footer.part.html'),
  :footer_ar => file_to_string('public/seeds/ara/footer.part.html'),
  :footer_he => file_to_string('public/seeds/heb/footer.part.html')
)

# Pages

Page.delete_all

Page.create!(
  :indicator   => Page::PAGES[:libertarian_result],

  :title       => 'O Libertarianismo',
  :content     => file_to_string('public/seeds/pt/libertarian.part.html'),

  :title_en    => 'The Libertarianism',
  :content_en  => file_to_string('public/seeds/en/libertarian.part.html'),

  :title_fr    => 'O Libertarianismo',
  :content_fr  => file_to_string('public/seeds/fr/libertarian.part.html'),

  :title_it    => 'O Libertarianismo',
  :content_it  => file_to_string('public/seeds/it/libertarian.part.html'),

  :title_es    => 'O Libertarianismo',
  :content_es  => file_to_string('public/seeds/es/libertarian.part.html'),

  :title_de    => 'O Libertarianismo',
  :content_de  => file_to_string('public/seeds/ge/libertarian.part.html'),

  :title_ru    => 'O Libertarianismo',
  :content_ru  => file_to_string('public/seeds/ru/libertarian.part.html'),

  :title_eo    => 'O Libertarianismo',
  :content_eo  => file_to_string('public/seeds/esp/libertarian.part.html'),

  :title_hi    => 'O Libertarianismo',
  :content_hi  => file_to_string('public/seeds/hi/libertarian.part.html'),

  :title_tr    => 'O Libertarianismo',
  :content_tr  => file_to_string('public/seeds/tu/libertarian.part.html'),

  :title_da    => 'O Libertarianismo',
  :content_da  => file_to_string('public/seeds/din/libertarian.part.html'),

  :title_sv    => 'O Libertarianismo',
  :content_sv  => file_to_string('public/seeds/sue/libertarian.part.html'),

  :title_zh    => 'O Libertarianismo',
  :content_zh  => file_to_string('public/seeds/ch/libertarian.part.html'),

  :title_ja    => 'O Libertarianismo',
  :content_ja  => file_to_string('public/seeds/jap/libertarian.part.html'),

  :title_ar    => 'O Libertarianismo',
  :content_ar  => file_to_string('public/seeds/ara/libertarian.part.html'),

  :title_he    => 'O Libertarianismo',
  :content_he  => file_to_string('public/seeds/heb/libertarian.part.html'),

  :published   => true
)

Page.create!(
  :indicator   => Page::PAGES[:right_result],

  :title       => 'A Direita',
  :content     => file_to_string('public/seeds/pt/right.part.html'),

  :title_en    => 'The Conservatives',
  :content_en  => file_to_string('public/seeds/en/right.part.html'),

  :title_fr    => 'A Direita',
  :content_fr  => file_to_string('public/seeds/fr/right.part.html'),

  :title_it    => 'A Direita',
  :content_it  => file_to_string('public/seeds/it/right.part.html'),

  :title_es    => 'A Direita',
  :content_es  => file_to_string('public/seeds/es/right.part.html'),

  :title_de    => 'A Direita',
  :content_de  => file_to_string('public/seeds/ge/right.part.html'),

  :title_ru    => 'A Direita',
  :content_ru  => file_to_string('public/seeds/ru/right.part.html'),

  :title_eo    => 'A Direita',
  :content_eo  => file_to_string('public/seeds/esp/right.part.html'),

  :title_hi    => 'A Direita',
  :content_hi  => file_to_string('public/seeds/hi/right.part.html'),

  :title_tr    => 'A Direita',
  :content_tr  => file_to_string('public/seeds/tu/right.part.html'),

  :title_da    => 'A Direita',
  :content_da  => file_to_string('public/seeds/din/right.part.html'),

  :title_sv    => 'A Direita',
  :content_sv  => file_to_string('public/seeds/sue/right.part.html'),

  :title_zh    => 'A Direita',
  :content_zh  => file_to_string('public/seeds/ch/right.part.html'),

  :title_ja    => 'A Direita',
  :content_ja  => file_to_string('public/seeds/jap/right.part.html'),

  :title_ar    => 'A Direita',
  :content_ar  => file_to_string('public/seeds/ara/right.part.html'),

  :title_he    => 'A Direita',
  :content_he  => file_to_string('public/seeds/heb/right.part.html'),

  :published   => true
)

Page.create!(
  :indicator   => Page::PAGES[:left_result],

  :title       => 'A Esquerda',
  :content     => file_to_string('public/seeds/pt/left.part.html'),

  :title_en    => 'The Liberals',
  :content_en  => file_to_string('public/seeds/en/left.part.html'),

  :title_fr    => 'A Esquerda',
  :content_fr  => file_to_string('public/seeds/fr/left.part.html'),

  :title_it    => 'A Esquerda',
  :content_it  => file_to_string('public/seeds/it/left.part.html'),

  :title_es    => 'A Esquerda',
  :content_es  => file_to_string('public/seeds/es/left.part.html'),

  :title_de    => 'A Esquerda',
  :content_de  => file_to_string('public/seeds/ge/left.part.html'),

  :title_ru    => 'A Esquerda',
  :content_ru  => file_to_string('public/seeds/ru/left.part.html'),

  :title_eo    => 'A Esquerda',
  :content_eo  => file_to_string('public/seeds/esp/left.part.html'),

  :title_hi    => 'A Esquerda',
  :content_hi  => file_to_string('public/seeds/hi/left.part.html'),

  :title_tr    => 'A Esquerda',
  :content_tr  => file_to_string('public/seeds/tu/left.part.html'),

  :title_da    => 'A Esquerda',
  :content_da  => file_to_string('public/seeds/din/left.part.html'),

  :title_sv    => 'A Esquerda',
  :content_sv  => file_to_string('public/seeds/sue/left.part.html'),

  :title_zh    => 'A Esquerda',
  :content_zh  => file_to_string('public/seeds/ch/left.part.html'),

  :title_ja    => 'A Esquerda',
  :content_ja  => file_to_string('public/seeds/jap/left.part.html'),

  :title_ar    => 'A Esquerda',
  :content_ar  => file_to_string('public/seeds/ara/left.part.html'),

  :title_he    => 'A Esquerda',
  :content_he  => file_to_string('public/seeds/heb/left.part.html'),

  :published   => true
)

Page.create!(
  :indicator   => Page::PAGES[:centrist_result],

  :title       => 'O Centrismo',
  :content     => file_to_string('public/seeds/pt/centrist.part.html'),

  :title_en    => 'The Centrism',
  :content_en  => file_to_string('public/seeds/en/centrist.part.html'),

  :title_fr    => 'O Centrismo',
  :content_fr  => file_to_string('public/seeds/fr/centrist.part.html'),

  :title_it    => 'O Centrismo',
  :content_it  => file_to_string('public/seeds/it/centrist.part.html'),

  :title_es    => 'O Centrismo',
  :content_es  => file_to_string('public/seeds/es/centrist.part.html'),

  :title_de    => 'O Centrismo',
  :content_de  => file_to_string('public/seeds/ge/centrist.part.html'),

  :title_ru    => 'O Centrismo',
  :content_ru  => file_to_string('public/seeds/ru/centrist.part.html'),

  :title_eo    => 'O Centrismo',
  :content_eo  => file_to_string('public/seeds/esp/centrist.part.html'),

  :title_hi    => 'O Centrismo',
  :content_hi  => file_to_string('public/seeds/hi/centrist.part.html'),

  :title_tr    => 'O Centrismo',
  :content_tr  => file_to_string('public/seeds/tu/centrist.part.html'),

  :title_da    => 'O Centrismo',
  :content_da  => file_to_string('public/seeds/din/centrist.part.html'),

  :title_sv    => 'O Centrismo',
  :content_sv  => file_to_string('public/seeds/sue/centrist.part.html'),

  :title_zh    => 'O Centrismo',
  :content_zh  => file_to_string('public/seeds/ch/centrist.part.html'),

  :title_ja    => 'O Centrismo',
  :content_ja  => file_to_string('public/seeds/jap/centrist.part.html'),

  :title_ar    => 'O Centrismo',
  :content_ar  => file_to_string('public/seeds/ara/centrist.part.html'),

  :title_he    => 'O Centrismo',
  :content_he  => file_to_string('public/seeds/heb/centrist.part.html'),

  :published   => true
)

Page.create!(
  :indicator   => Page::PAGES[:statist_result],

  :title       => 'O Estatismo',
  :content     => file_to_string('public/seeds/pt/statist.part.html'),

  :title_en    => 'The Statism',
  :content_en  => file_to_string('public/seeds/en/statist.part.html'),

  :title_fr    => 'O Estatismo',
  :content_fr  => file_to_string('public/seeds/fr/statist.part.html'),

  :title_it    => 'O Estatismo',
  :content_it  => file_to_string('public/seeds/it/statist.part.html'),

  :title_es    => 'O Estatismo',
  :content_es  => file_to_string('public/seeds/es/statist.part.html'),

  :title_de    => 'O Estatismo',
  :content_de  => file_to_string('public/seeds/ge/statist.part.html'),

  :title_ru    => 'O Estatismo',
  :content_ru  => file_to_string('public/seeds/ru/statist.part.html'),

  :title_eo    => 'O Estatismo',
  :content_eo  => file_to_string('public/seeds/esp/statist.part.html'),

  :title_hi    => 'O Estatismo',
  :content_hi  => file_to_string('public/seeds/hi/statist.part.html'),

  :title_tr    => 'O Estatismo',
  :content_tr  => file_to_string('public/seeds/tu/statist.part.html'),

  :title_da    => 'O Estatismo',
  :content_da  => file_to_string('public/seeds/din/statist.part.html'),

  :title_sv    => 'O Estatismo',
  :content_sv  => file_to_string('public/seeds/sue/statist.part.html'),

  :title_zh    => 'O Estatismo',
  :content_zh  => file_to_string('public/seeds/ch/statist.part.html'),

  :title_ja    => 'O Estatismo',
  :content_ja  => file_to_string('public/seeds/jap/statist.part.html'),

  :title_ar    => 'O Estatismo',
  :content_ar  => file_to_string('public/seeds/ara/statist.part.html'),

  :title_he    => 'O Estatismo',
  :content_he  => file_to_string('public/seeds/heb/statist.part.html'),

  :published   => true
)

Page.create!(
  :indicator => Page::PAGES[:partners],
  :title => 'Apoiadores',
  :content => file_to_string('public/seeds/partners.part.html'),
  :published => true
)

Page.create!(
  :indicator   => Page::PAGES[:about],

  :title       => 'O que é o Diagrama de Nolan?',
  :content     => file_to_string('public/seeds/pt/about.part.html'),

  :title_en    => 'What is the Nolan Chart?',
  :content_en  => file_to_string('public/seeds/en/about.part.html'),

  :title_fr    => 'O que é o Diagrama de Nolan?',
  :content_fr  => file_to_string('public/seeds/fr/about.part.html'),

  :title_it    => 'O que é o Diagrama de Nolan?',
  :content_it  => file_to_string('public/seeds/it/about.part.html'),

  :title_es    => 'O que é o Diagrama de Nolan?',
  :content_es  => file_to_string('public/seeds/es/about.part.html'),

  :title_de    => 'O que é o Diagrama de Nolan?',
  :content_de  => file_to_string('public/seeds/ge/about.part.html'),

  :title_ru    => 'O que é o Diagrama de Nolan?',
  :content_ru  => file_to_string('public/seeds/ru/about.part.html'),

  :title_eo    => 'O que é o Diagrama de Nolan?',
  :content_eo  => file_to_string('public/seeds/esp/about.part.html'),

  :title_hi    => 'O que é o Diagrama de Nolan?',
  :content_hi  => file_to_string('public/seeds/hi/about.part.html'),

  :title_tr    => 'O que é o Diagrama de Nolan?',
  :content_tr  => file_to_string('public/seeds/tu/about.part.html'),

  :title_da    => 'O que é o Diagrama de Nolan?',
  :content_da  => file_to_string('public/seeds/din/about.part.html'),

  :title_sv    => 'O que é o Diagrama de Nolan?',
  :content_sv  => file_to_string('public/seeds/sue/about.part.html'),

  :title_zh    => 'O que é o Diagrama de Nolan?',
  :content_zh  => file_to_string('public/seeds/ch/about.part.html'),

  :title_ja    => 'O que é o Diagrama de Nolan?',
  :content_ja  => file_to_string('public/seeds/jap/about.part.html'),

  :title_ar    => 'O que é o Diagrama de Nolan?',
  :content_ar  => file_to_string('public/seeds/ara/about.part.html'),

  :title_he    => 'O que é o Diagrama de Nolan?',
  :content_he  => file_to_string('public/seeds/heb/about.part.html'),

  :published   => true
)

Page.create!(
  :indicator => Page::PAGES[:lecture],

  :title       => 'Solicite uma palestra',
  :content     => file_to_string('public/seeds/pt/lecture.part.html'),

  :title_en    => 'Request a talk',
  :content_en  => file_to_string('public/seeds/en/lecture.part.html'),

  :title_fr    => 'Solicite uma palestra',
  :content_fr  => file_to_string('public/seeds/fr/lecture.part.html'),

  :title_it    => 'Solicite uma palestra',
  :content_it  => file_to_string('public/seeds/it/lecture.part.html'),

  :title_es    => 'Solicite uma palestra',
  :content_es  => file_to_string('public/seeds/es/lecture.part.html'),

  :title_de    => 'Solicite uma palestra',
  :content_de  => file_to_string('public/seeds/ge/lecture.part.html'),

  :title_ru    => 'Solicite uma palestra',
  :content_ru  => file_to_string('public/seeds/ru/lecture.part.html'),

  :title_eo    => 'Solicite uma palestra',
  :content_eo  => file_to_string('public/seeds/esp/lecture.part.html'),

  :title_hi    => 'Solicite uma palestra',
  :content_hi  => file_to_string('public/seeds/hi/lecture.part.html'),

  :title_tr    => 'Solicite uma palestra',
  :content_tr  => file_to_string('public/seeds/tu/lecture.part.html'),

  :title_da    => 'Solicite uma palestra',
  :content_da  => file_to_string('public/seeds/din/lecture.part.html'),

  :title_sv    => 'Solicite uma palestra',
  :content_sv  => file_to_string('public/seeds/sue/lecture.part.html'),

  :title_zh    => 'Solicite uma palestra',
  :content_zh  => file_to_string('public/seeds/ch/lecture.part.html'),

  :title_ja    => 'Solicite uma palestra',
  :content_ja  => file_to_string('public/seeds/jap/lecture.part.html'),

  :title_ar    => 'Solicite uma palestra',
  :content_ar  => file_to_string('public/seeds/ara/lecture.part.html'),

  :title_he    => 'Solicite uma palestra',
  :content_he  => file_to_string('public/seeds/heb/lecture.part.html'),

  :published   => true
)

Page.create!(
  :indicator => Page::PAGES[:donate],

  :title       => 'Faça uma doação',
  :content     => file_to_string('public/seeds/pt/donate.part.html'),

  :title_en    => 'Make a Donation',
  :content_en  => file_to_string('public/seeds/en/donate.part.html'),

  :title_fr    => 'Faça uma doação',
  :content_fr  => file_to_string('public/seeds/fr/donate.part.html'),

  :title_it    => 'Faça uma doação',
  :content_it  => file_to_string('public/seeds/it/donate.part.html'),

  :title_es    => 'Faça uma doação',
  :content_es  => file_to_string('public/seeds/es/donate.part.html'),

  :title_de    => 'Faça uma doação',
  :content_de  => file_to_string('public/seeds/ge/donate.part.html'),

  :title_ru    => 'Faça uma doação',
  :content_ru  => file_to_string('public/seeds/ru/donate.part.html'),

  :title_eo    => 'Faça uma doação',
  :content_eo  => file_to_string('public/seeds/esp/donate.part.html'),

  :title_hi    => 'Faça uma doação',
  :content_hi  => file_to_string('public/seeds/hi/donate.part.html'),

  :title_tr    => 'Faça uma doação',
  :content_tr  => file_to_string('public/seeds/tu/donate.part.html'),

  :title_da    => 'Faça uma doação',
  :content_da  => file_to_string('public/seeds/din/donate.part.html'),

  :title_sv    => 'Faça uma doação',
  :content_sv  => file_to_string('public/seeds/sue/donate.part.html'),

  :title_zh    => 'Faça uma doação',
  :content_zh  => file_to_string('public/seeds/ch/donate.part.html'),

  :title_ja    => 'Faça uma doação',
  :content_ja  => file_to_string('public/seeds/jap/donate.part.html'),

  :title_ar    => 'Faça uma doação',
  :content_ar  => file_to_string('public/seeds/ara/donate.part.html'),

  :title_he    => 'Faça uma doação',
  :content_he  => file_to_string('public/seeds/heb/donate.part.html'),

  :published   => true
)

# Sponsors

Sponsor.delete_all

Sponsor.create!(
  :name => 'OrdemLivre.org',
  :link => 'http://www.ordemlivre.org/',
  :image => File.open('public/images/ordemlivre_logo.png')
)

# Questions

Question.delete_all

Question.create!(
  :question    => 'O governo deve regular a imprensa e a internet.',
  :question_en => 'The government should regulate the media and the internet.',
  :question_fr => 'O governo deve regular a imprensa e a internet.',
  :question_it => 'O governo deve regular a imprensa e a internet.',
  :question_es => 'O governo deve regular a imprensa e a internet.',
  :question_de => 'O governo deve regular a imprensa e a internet.',
  :question_ru => 'O governo deve regular a imprensa e a internet.',
  :question_eo => 'O governo deve regular a imprensa e a internet.',
  :question_hi => 'O governo deve regular a imprensa e a internet.',
  :question_tr => 'O governo deve regular a imprensa e a internet.',
  :question_da => 'O governo deve regular a imprensa e a internet.',
  :question_sv => 'O governo deve regular a imprensa e a internet.',
  :question_zh => 'O governo deve regular a imprensa e a internet.',
  :question_ja => 'O governo deve regular a imprensa e a internet.',
  :question_ar => 'O governo deve regular a imprensa e a internet.',
  :question_he => 'O governo deve regular a imprensa e a internet.',

  :category    => Question::CATEGORY[:civil_liberties],
  :weight      => 10
)

Question.create!(
  :question    => 'O alistamento militar deve ser obrigatório.',
  :question_en => 'The conscription should be mandatory.',
  :question_fr => 'O alistamento militar deve ser obrigatório.',
  :question_it => 'O alistamento militar deve ser obrigatório.',
  :question_es => 'O alistamento militar deve ser obrigatório.',
  :question_de => 'O alistamento militar deve ser obrigatório.',
  :question_ru => 'O alistamento militar deve ser obrigatório.',
  :question_eo => 'O alistamento militar deve ser obrigatório.',
  :question_hi => 'O alistamento militar deve ser obrigatório.',
  :question_tr => 'O alistamento militar deve ser obrigatório.',
  :question_da => 'O alistamento militar deve ser obrigatório.',
  :question_sv => 'O alistamento militar deve ser obrigatório.',
  :question_zh => 'O alistamento militar deve ser obrigatório.',
  :question_ja => 'O alistamento militar deve ser obrigatório.',
  :question_ar => 'O alistamento militar deve ser obrigatório.',
  :question_he => 'O alistamento militar deve ser obrigatório.',

  :category    => Question::CATEGORY[:civil_liberties],
  :weight      => 9
)

Question.create!(
  :question    => 'Deve ser feita uma seleção dos estrangeiros que desejarem morar no Brasil.',
  :question_en => 'There should be a selection of foreigners wishing to live in your country.',
  :question_fr => 'Deve ser feita uma seleção dos estrangeiros que desejarem morar no Brasil.',
  :question_it => 'Deve ser feita uma seleção dos estrangeiros que desejarem morar no Brasil.',
  :question_es => 'Deve ser feita uma seleção dos estrangeiros que desejarem morar no Brasil.',
  :question_de => 'Deve ser feita uma seleção dos estrangeiros que desejarem morar no Brasil.',
  :question_ru => 'Deve ser feita uma seleção dos estrangeiros que desejarem morar no Brasil.',
  :question_eo => 'Deve ser feita uma seleção dos estrangeiros que desejarem morar no Brasil.',
  :question_hi => 'Deve ser feita uma seleção dos estrangeiros que desejarem morar no Brasil.',
  :question_tr => 'Deve ser feita uma seleção dos estrangeiros que desejarem morar no Brasil.',
  :question_da => 'Deve ser feita uma seleção dos estrangeiros que desejarem morar no Brasil.',
  :question_sv => 'Deve ser feita uma seleção dos estrangeiros que desejarem morar no Brasil.',
  :question_zh => 'Deve ser feita uma seleção dos estrangeiros que desejarem morar no Brasil.',
  :question_ja => 'Deve ser feita uma seleção dos estrangeiros que desejarem morar no Brasil.',
  :question_ar => 'Deve ser feita uma seleção dos estrangeiros que desejarem morar no Brasil.',
  :question_he => 'Deve ser feita uma seleção dos estrangeiros que desejarem morar no Brasil.',

  :category    => Question::CATEGORY[:civil_liberties],
  :weight      => 8
)

Question.create!(
  :question    => 'A produção, comercialização e uso de drogas devem ser combatidos.',
  :question_en => 'The production, marketing and use of drugs must be fought.',
  :question_fr => 'A produção, comercialização e uso de drogas devem ser combatidos.',
  :question_it => 'A produção, comercialização e uso de drogas devem ser combatidos.',
  :question_es => 'A produção, comercialização e uso de drogas devem ser combatidos.',
  :question_de => 'A produção, comercialização e uso de drogas devem ser combatidos.',
  :question_ru => 'A produção, comercialização e uso de drogas devem ser combatidos.',
  :question_eo => 'A produção, comercialização e uso de drogas devem ser combatidos.',
  :question_hi => 'A produção, comercialização e uso de drogas devem ser combatidos.',
  :question_tr => 'A produção, comercialização e uso de drogas devem ser combatidos.',
  :question_da => 'A produção, comercialização e uso de drogas devem ser combatidos.',
  :question_sv => 'A produção, comercialização e uso de drogas devem ser combatidos.',
  :question_zh => 'A produção, comercialização e uso de drogas devem ser combatidos.',
  :question_ja => 'A produção, comercialização e uso de drogas devem ser combatidos.',
  :question_ar => 'A produção, comercialização e uso de drogas devem ser combatidos.',
  :question_he => 'A produção, comercialização e uso de drogas devem ser combatidos.',

  :category    => Question::CATEGORY[:civil_liberties],
  :weight      => 7
)

Question.create!(
  :question    => 'A discriminação deve ser considerada um crime.',
  :question_en => 'Discrimination should be considered a crime.',
  :question_fr => 'A discriminação deve ser considerada um crime.',
  :question_it => 'A discriminação deve ser considerada um crime.',
  :question_es => 'A discriminação deve ser considerada um crime.',
  :question_de => 'A discriminação deve ser considerada um crime.',
  :question_ru => 'A discriminação deve ser considerada um crime.',
  :question_eo => 'A discriminação deve ser considerada um crime.',
  :question_hi => 'A discriminação deve ser considerada um crime.',
  :question_tr => 'A discriminação deve ser considerada um crime.',
  :question_da => 'A discriminação deve ser considerada um crime.',
  :question_sv => 'A discriminação deve ser considerada um crime.',
  :question_zh => 'A discriminação deve ser considerada um crime.',
  :question_ja => 'A discriminação deve ser considerada um crime.',
  :question_ar => 'A discriminação deve ser considerada um crime.',
  :question_he => 'A discriminação deve ser considerada um crime.',

  :category    => Question::CATEGORY[:civil_liberties],
  :weight      => 6
)

Question.create!(
  :question    => 'O governo pode cobrar altos impostos se os serviços prestados forem adequados.',
  :question_en => 'The government can charge higher taxes if the services are adequate.',
  :question_fr => 'O governo pode cobrar altos impostos se os serviços prestados forem adequados.',
  :question_it => 'O governo pode cobrar altos impostos se os serviços prestados forem adequados.',
  :question_es => 'O governo pode cobrar altos impostos se os serviços prestados forem adequados.',
  :question_de => 'O governo pode cobrar altos impostos se os serviços prestados forem adequados.',
  :question_ru => 'O governo pode cobrar altos impostos se os serviços prestados forem adequados.',
  :question_eo => 'O governo pode cobrar altos impostos se os serviços prestados forem adequados.',
  :question_hi => 'O governo pode cobrar altos impostos se os serviços prestados forem adequados.',
  :question_tr => 'O governo pode cobrar altos impostos se os serviços prestados forem adequados.',
  :question_da => 'O governo pode cobrar altos impostos se os serviços prestados forem adequados.',
  :question_sv => 'O governo pode cobrar altos impostos se os serviços prestados forem adequados.',
  :question_zh => 'O governo pode cobrar altos impostos se os serviços prestados forem adequados.',
  :question_ja => 'O governo pode cobrar altos impostos se os serviços prestados forem adequados.',
  :question_ar => 'O governo pode cobrar altos impostos se os serviços prestados forem adequados.',
  :question_he => 'O governo pode cobrar altos impostos se os serviços prestados forem adequados.',

  :category    => Question::CATEGORY[:economic_freedom],
  :weight      => 5
)

Question.create!(
  :question    => 'Deve haver salário mínimo determinado pelo governo.',
  :question_en => 'There should be a minimum wage determined by the government.',
  :question_fr => 'Deve haver salário mínimo determinado pelo governo.',
  :question_it => 'Deve haver salário mínimo determinado pelo governo.',
  :question_es => 'Deve haver salário mínimo determinado pelo governo.',
  :question_de => 'Deve haver salário mínimo determinado pelo governo.',
  :question_ru => 'Deve haver salário mínimo determinado pelo governo.',
  :question_eo => 'Deve haver salário mínimo determinado pelo governo.',
  :question_hi => 'Deve haver salário mínimo determinado pelo governo.',
  :question_tr => 'Deve haver salário mínimo determinado pelo governo.',
  :question_da => 'Deve haver salário mínimo determinado pelo governo.',
  :question_sv => 'Deve haver salário mínimo determinado pelo governo.',
  :question_zh => 'Deve haver salário mínimo determinado pelo governo.',
  :question_ja => 'Deve haver salário mínimo determinado pelo governo.',
  :question_ar => 'Deve haver salário mínimo determinado pelo governo.',
  :question_he => 'Deve haver salário mínimo determinado pelo governo.',

  :category    => Question::CATEGORY[:economic_freedom],
  :weight      => 4
)

Question.create!(
  :question    => 'O governo deve resgatar empresas em dificuldade financeira.',
  :question_en => 'The government must rescue companies in financial difficulty.',
  :question_fr => 'O governo deve resgatar empresas em dificuldade financeira.',
  :question_it => 'O governo deve resgatar empresas em dificuldade financeira.',
  :question_es => 'O governo deve resgatar empresas em dificuldade financeira.',
  :question_de => 'O governo deve resgatar empresas em dificuldade financeira.',
  :question_ru => 'O governo deve resgatar empresas em dificuldade financeira.',
  :question_eo => 'O governo deve resgatar empresas em dificuldade financeira.',
  :question_hi => 'O governo deve resgatar empresas em dificuldade financeira.',
  :question_tr => 'O governo deve resgatar empresas em dificuldade financeira.',
  :question_da => 'O governo deve resgatar empresas em dificuldade financeira.',
  :question_sv => 'O governo deve resgatar empresas em dificuldade financeira.',
  :question_zh => 'O governo deve resgatar empresas em dificuldade financeira.',
  :question_ja => 'O governo deve resgatar empresas em dificuldade financeira.',
  :question_ar => 'O governo deve resgatar empresas em dificuldade financeira.',
  :question_he => 'O governo deve resgatar empresas em dificuldade financeira.',

  :category    => Question::CATEGORY[:economic_freedom],
  :weight      => 3
)

Question.create!(
  :question    => 'O governo deve criar agências para regular o setor privado.',
  :question_en => 'The government must establish agencies to regulate the private sector.',
  :question_fr => 'O governo deve criar agências para regular o setor privado.',
  :question_it => 'O governo deve criar agências para regular o setor privado.',
  :question_es => 'O governo deve criar agências para regular o setor privado.',
  :question_de => 'O governo deve criar agências para regular o setor privado.',
  :question_ru => 'O governo deve criar agências para regular o setor privado.',
  :question_eo => 'O governo deve criar agências para regular o setor privado.',
  :question_hi => 'O governo deve criar agências para regular o setor privado.',
  :question_tr => 'O governo deve criar agências para regular o setor privado.',
  :question_da => 'O governo deve criar agências para regular o setor privado.',
  :question_sv => 'O governo deve criar agências para regular o setor privado.',
  :question_zh => 'O governo deve criar agências para regular o setor privado.',
  :question_ja => 'O governo deve criar agências para regular o setor privado.',
  :question_ar => 'O governo deve criar agências para regular o setor privado.',
  :question_he => 'O governo deve criar agências para regular o setor privado.',

  :category    => Question::CATEGORY[:economic_freedom],
  :weight      => 2
)

Question.create!(
  :question    => 'O governo deve usar os impostos para fazer distribuição de renda.',
  :question_en => 'The government must use taxes to make distribution of wealth.',
  :question_fr => 'O governo deve usar os impostos para fazer distribuição de renda.',
  :question_it => 'O governo deve usar os impostos para fazer distribuição de renda.',
  :question_es => 'O governo deve usar os impostos para fazer distribuição de renda.',
  :question_de => 'O governo deve usar os impostos para fazer distribuição de renda.',
  :question_ru => 'O governo deve usar os impostos para fazer distribuição de renda.',
  :question_eo => 'O governo deve usar os impostos para fazer distribuição de renda.',
  :question_hi => 'O governo deve usar os impostos para fazer distribuição de renda.',
  :question_tr => 'O governo deve usar os impostos para fazer distribuição de renda.',
  :question_da => 'O governo deve usar os impostos para fazer distribuição de renda.',
  :question_sv => 'O governo deve usar os impostos para fazer distribuição de renda.',
  :question_zh => 'O governo deve usar os impostos para fazer distribuição de renda.',
  :question_ja => 'O governo deve usar os impostos para fazer distribuição de renda.',
  :question_ar => 'O governo deve usar os impostos para fazer distribuição de renda.',
  :question_he => 'O governo deve usar os impostos para fazer distribuição de renda.',

  :category    => Question::CATEGORY[:economic_freedom],
  :weight      => 1
)

Report.delete_all
Report.create(:name => 'Porcentagem de posições políticas')

p 'Cidades'

City.delete_all
open("#{Rails.root}/db/cities_seed.txt") do |cities|
  cities.read.each_line do |city|
    if city != "\n"
      state, name = city.chomp.split("|")
      City.create!(:name => "#{name} (#{state})")
    end
  end
end
