
class ChangeDefaultParticipantCountryField < ActiveRecord::Migration
  def up
  	change_column :participants, :country, :string, :default => 'Brazil'
  end
end
