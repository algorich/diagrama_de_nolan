class Report < ActiveRecord::Migration
  def up
  	create_table :reports do |t|
  		t.string :name
    end
  end

  def down
  	drop_table :reports
  end
end
