class AddFieldsToConfiguration < ActiveRecord::Migration
  def self.up
    execute "ALTER TABLE configurations ENGINE='MyISAM';"

    add_column :configurations, :footer_en, :text
    add_column :configurations, :footer_fr, :text
    add_column :configurations, :footer_it, :text
    add_column :configurations, :footer_es, :text
    add_column :configurations, :footer_de, :text
    add_column :configurations, :footer_ru, :text
    add_column :configurations, :footer_eo, :text
    add_column :configurations, :footer_hi, :text
    add_column :configurations, :footer_tr, :text
    add_column :configurations, :footer_da, :text
    add_column :configurations, :footer_sv, :text
    add_column :configurations, :footer_zh, :text
    add_column :configurations, :footer_ja, :text
    add_column :configurations, :footer_ar, :text
    add_column :configurations, :footer_he, :text
  end

  def self.down
    remove_column :configurations, :footer_en
    remove_column :configurations, :footer_fr
    remove_column :configurations, :footer_it
    remove_column :configurations, :footer_es
    remove_column :configurations, :footer_de
    remove_column :configurations, :footer_ru
    remove_column :configurations, :footer_eo
    remove_column :configurations, :footer_hi
    remove_column :configurations, :footer_tr
    remove_column :configurations, :footer_da
    remove_column :configurations, :footer_sv
    remove_column :configurations, :footer_zh
    remove_column :configurations, :footer_ja
    remove_column :configurations, :footer_ar
    remove_column :configurations, :footer_he
  end
end
