class AddPageTrackerToConfigurations < ActiveRecord::Migration
  def self.up
    add_column :configurations, :google_analytics_page_tracker, :text
  end

  def self.down
    remove_column :configurations, :google_analytics_page_tracker
  end
end
