class AddLangsToConfiguration < ActiveRecord::Migration
  def self.up
    add_column :configurations, :en_lang, :boolean
    add_column :configurations, :fr_lang, :boolean
    add_column :configurations, :it_lang, :boolean
    add_column :configurations, :es_lang, :boolean
    add_column :configurations, :de_lang, :boolean
    add_column :configurations, :ru_lang, :boolean
    add_column :configurations, :eo_lang, :boolean
    add_column :configurations, :hi_lang, :boolean
    add_column :configurations, :tr_lang, :boolean
    add_column :configurations, :da_lang, :boolean
    add_column :configurations, :sv_lang, :boolean
    add_column :configurations, :zh_lang, :boolean
    add_column :configurations, :ja_lang, :boolean
    add_column :configurations, :ar_lang, :boolean
    add_column :configurations, :he_lang, :boolean
  end

  def self.down
    remove_column :configurations, :en_lang
    remove_column :configurations, :fr_lang
    remove_column :configurations, :it_lang
    remove_column :configurations, :es_lang
    remove_column :configurations, :de_lang
    remove_column :configurations, :ru_lang
    remove_column :configurations, :eo_lang
    remove_column :configurations, :hi_lang
    remove_column :configurations, :tr_lang
    remove_column :configurations, :da_lang
    remove_column :configurations, :sv_lang
    remove_column :configurations, :zh_lang
    remove_column :configurations, :ja_lang
    remove_column :configurations, :ar_lang
    remove_column :configurations, :he_lang
  end
end