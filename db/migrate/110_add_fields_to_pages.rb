class AddFieldsToPages < ActiveRecord::Migration
  def self.up
    execute "ALTER TABLE pages ENGINE='MyISAM';"

    add_column :pages, :title_en, :string
    add_column :pages, :content_en, :text

    add_column :pages, :title_fr, :string
    add_column :pages, :content_fr, :text

    add_column :pages, :title_it, :string
    add_column :pages, :content_it, :text

    add_column :pages, :title_es, :string
    add_column :pages, :content_es, :text

    add_column :pages, :title_de, :string
    add_column :pages, :content_de, :text

    add_column :pages, :title_ru, :string
    add_column :pages, :content_ru, :text

    add_column :pages, :title_eo, :string
    add_column :pages, :content_eo, :text

    add_column :pages, :title_hi, :string
    add_column :pages, :content_hi, :text

    add_column :pages, :title_tr, :string
    add_column :pages, :content_tr, :text

    add_column :pages, :title_da, :string
    add_column :pages, :content_da, :text

    add_column :pages, :title_sv, :string
    add_column :pages, :content_sv, :text

    add_column :pages, :title_zh, :string
    add_column :pages, :content_zh, :text

    add_column :pages, :title_ja, :string
    add_column :pages, :content_ja, :text

    add_column :pages, :title_ar, :string
    add_column :pages, :content_ar, :text

    add_column :pages, :title_he, :string
    add_column :pages, :content_he, :text
  end

  def self.down
    remove_column :pages, :title_en
    remove_column :pages, :content_en

    remove_column :pages, :title_fr
    remove_column :pages, :content_fr

    remove_column :pages, :title_it
    remove_column :pages, :content_it

    remove_column :pages, :title_es
    remove_column :pages, :content_es

    remove_column :pages, :title_de
    remove_column :pages, :content_de

    remove_column :pages, :title_ru
    remove_column :pages, :content_ru

    remove_column :pages, :title_eo
    remove_column :pages, :content_eo

    remove_column :pages, :title_hi
    remove_column :pages, :content_hi

    remove_column :pages, :title_tr
    remove_column :pages, :content_tr

    remove_column :pages, :title_da
    remove_column :pages, :content_da

    remove_column :pages, :title_sv
    remove_column :pages, :content_sv

    remove_column :pages, :title_zh
    remove_column :pages, :content_zh

    remove_column :pages, :title_ja
    remove_column :pages, :content_ja

    remove_column :pages, :title_ar
    remove_column :pages, :content_ar

    remove_column :pages, :title_he
    remove_column :pages, :content_he
  end
end
