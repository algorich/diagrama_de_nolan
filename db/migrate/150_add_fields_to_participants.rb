class AddFieldsToParticipants < ActiveRecord::Migration
  def self.up
    add_column :participants, :country, :string
    add_column :participants, :position_result, :int
    add_column :participants, :economic_freedom_score, :int
    add_column :participants, :civil_liberty_score, :int
  end

  def self.down
    remove_column :participants, :country
    remove_column :participants, :position_result
    remove_column :participants, :economic_freedom_score
    remove_column :participants, :civil_liberty_score
  end
end
