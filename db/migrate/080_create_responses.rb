# -*- encoding : utf-8 -*-
class CreateResponses < ActiveRecord::Migration
  def self.up
    create_table :responses do |t|
      t.references :participant
      t.references :question
      t.integer :response

      t.timestamps
    end
  end

  def self.down
    drop_table :responses
  end
end

