class AddCityNameToParticipants < ActiveRecord::Migration
  def change
    add_column :participants, :city_name, :string
    Participant.reset_column_information
    Participant.includes(:city).each do |p|
      p.city_name = p.city.try(:name)
      p.save
    end
  end
end
