# -*- encoding : utf-8 -*-

class CreateParticipants < ActiveRecord::Migration
  def self.up
    create_table :participants do |t|
      t.string :name
      t.string :email
      t.boolean :receive_email, :default => true
      t.references :city

      t.timestamps
    end
  end

  def self.down
    drop_table :participants
  end
end
