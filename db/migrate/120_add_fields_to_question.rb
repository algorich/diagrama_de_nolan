class AddFieldsToQuestion < ActiveRecord::Migration
  def self.up
    add_column :questions, :question_en, :text
    add_column :questions, :question_fr, :text
    add_column :questions, :question_it, :text
    add_column :questions, :question_es, :text
    add_column :questions, :question_de, :text
    add_column :questions, :question_ru, :text
    add_column :questions, :question_eo, :text
    add_column :questions, :question_hi, :text
    add_column :questions, :question_tr, :text
    add_column :questions, :question_da, :text
    add_column :questions, :question_sv, :text
    add_column :questions, :question_zh, :text
    add_column :questions, :question_ja, :text
    add_column :questions, :question_ar, :text
    add_column :questions, :question_he, :text
  end

  def self.down
    remove_column :questions, :question_en
    remove_column :questions, :question_fr
    remove_column :questions, :question_it
    remove_column :questions, :question_es
    remove_column :questions, :question_de
    remove_column :questions, :question_ru
    remove_column :questions, :question_eo
    remove_column :questions, :question_hi
    remove_column :questions, :question_tr
    remove_column :questions, :question_da
    remove_column :questions, :question_sv
    remove_column :questions, :question_zh
    remove_column :questions, :question_ja
    remove_column :questions, :question_ar
    remove_column :questions, :question_he
  end
end
