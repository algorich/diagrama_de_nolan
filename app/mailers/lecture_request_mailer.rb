# -*- encoding : utf-8 -*-
class LectureRequestMailer < ActionMailer::Base
  def lecture_request(params={})
    @name = params[:name]
    @email = params[:email]
    @phone = params[:phone]
    @city = params[:city]
    @state = params[:state]
    @place = params[:place]
    @to = params[:to]
    mail(:from => @email,
         :to => @to,
         :subject => "[Solicitação de palestra] #{@name}",
         :content_type => "text/plain")
  end
end

