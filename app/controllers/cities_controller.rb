class CitiesController < ApplicationController
  def index
    respond_to do |format|
      format.json { render :json => City.where("name like ?", "%#{params[:q]}%").limit(15) }
    end
  end
end
