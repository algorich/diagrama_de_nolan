# -*- encoding : utf-8 -*-
class SiteController < ApplicationController
  # Used to redirect a users that enters "nolan.estudantespelaliberdade.com.br"
  # based on his IP.

  before_filter :set_initial_locale, :except => [:responses_create, :send_lecture_request]

  def set_initial_locale
    (redirect_to "#{request.url}?locale=#{lookup}" and return) if request.url !~ /locale\=/
  end

  layout 'fb_redirector', :only => :result_redirector

  def index
  end

  # LECTURE REQUEST BEGIN

  def lecture_request
    @lecture_request = Page.find_by_indicator(Page::PAGES[:lecture])
    @lecture_request_form = LectureRequest.new

    raise ActionController::RoutingError unless @lecture_request.published
  end

  def send_lecture_request
    @lecture_request = Page.where('indicator = ?', Page::PAGES[:lecture]).first
    @lecture_request_form = LectureRequest.new(params[:lecture_request])

    if @lecture_request_form.save
      redirect_to(lecture_request_path,
        :notice => t('lecture.error_request'))
    else
      render :action => :lecture_request
    end
  end

  # LECTURE REQUEST END

  # QUESTIONNAIRE BEGIN

  def responses_new
    @questions_civil_liberties = Question.
      where('category = ?', Question::CATEGORY[:civil_liberties]).
      order('weight DESC').limit(5)
    @questions_economic_freendom = Question.
      where('category = ?', Question::CATEGORY[:economic_freedom]).
      order('weight DESC').limit(5)
    @participant = Participant.new
  end

  def validate_responses
    @current_step = params[:current_step]
    @responses = []
    params[:questionnaire][:responses].each do |question_index, response|
      @responses << Response.new(:response => response.last)
    end
    respond_to do |format|
      format.js
    end
  end

  def responses_create
    @participant = Participant.new(params[:questionnaire][:participant])
    @results = { :economic_score => 0, :civil_score => 0 }

    if @participant.valid?
      @questions_category_1 = Question.
        where('category = ?', Question::CATEGORY[:civil_liberties]).
        order('weight DESC').limit(5)
      @questions_category_2 = Question.
        where('category = ?', Question::CATEGORY[:economic_freedom]).
        order('weight DESC').limit(5)
      @responses = []

      @questions_category_1.each_with_index do |question, i|
        i += 1
        response = Response.new(
          :participant => @participant,
          :question => question,
          :response => params[:questionnaire][:responses]["question_#{i}"]
        )

        @responses << response
        @results[:civil_score] += response.response
      end

      @questions_category_2.each_with_index do |question, i|
        i += 6
        response = Response.new(
          :participant => @participant,
          :question => question,
          :response => params[:questionnaire][:responses]["question_#{i}"]
        )

        @responses << response
        @results[:economic_score] += response.response
      end

      @responses.each { |r| r.save }

      @participant.economic_freedom_score = @results[:economic_score]
      @participant.civil_liberty_score = @results[:civil_score]
      @participant.save
    end
  end

  def result
  end

  # QUESTIONNAIRE END

  # PAGE BEGIN

  def page
    Page.where('published = ?', true).each do |p|
      @page = p if p.title.parameterize == params[:page]
    end

    raise ActionController::RoutingError if @page.nil?
  end

  # PAGE END

  def result_redirector
    @position = params[:position]

    if params[:locale] == 'pt'
      @image = {
        :libertarian => '/assets/fb_libertarian.jpg',
        :left => '/assets/fb_left.jpg',
        :centrist => '/assets/fb_centrist.jpg',
        :right => '/assets/fb_right.jpg',
        :statist => '/assets/fb_statist.jpg'
      }
    else
      @image = {
        :libertarian => '/assets/fb_int_libertarian.jpg',
        :left => '/assets/fb_int_left.jpg',
        :centrist => '/assets/fb_int_centrist.jpg',
        :right => '/assets/fb_int_right.jpg',
        :statist => '/assets/fb_int_statist.jpg'
      }
    end
  end


  protected
  def lookup
    request.compatible_language_from(ActiveRecord::Base::Configuration.first.valid_langs)
  end
end
