# -*- encoding : utf-8 -*-

class ApplicationController < ActionController::Base
  protect_from_forgery

  layout :layout_by_resource

  before_filter :set_locale

  # before_filter :mock_ip_address

  def default_url_options(options={})
    { :locale => I18n.locale }
  end

  def set_locale
    I18n.locale = 'pt'

    if !params[:locale].blank? and ActiveRecord::Base::Configuration.first.valid_langs.include? params[:locale]
      I18n.locale = params[:locale]
    end
  end

  protected

  def layout_by_resource
    if devise_controller?
      'login'
    else
      'application'
    end
  end

  # def mock_ip_address
  #   if Rails.env == 'development'
  #     request.instance_eval <<-EOS
  #       def ip
  #         "189.25.3.76"
  #       end
  #     EOS
  #   end
  # end

end
