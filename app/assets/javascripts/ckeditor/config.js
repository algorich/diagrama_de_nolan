﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.language = 'pt-br';
    config.toolbar = 'Full';

    config.toolbar_Full =
    [
        ['Maximize'],
        ['Cut','Copy','Paste','PasteText'],
        ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
        ['ShowBlocks', 'Source'],
        '/',
        ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
        ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
        ['Link','Unlink'],
        ['Image','Table','HorizontalRule','SpecialChar'],
    ];

    config.toolbar_Basic =
    [
        ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink','-','About']
    ];

    // Upload configuration
    config.filebrowserBrowseUrl = '/ckeditor/attachment_files';
    config.filebrowserUploadUrl = '/ckeditor/attachment_files';
    config.filebrowserImageBrowseUrl = '/ckeditor/pictures';
    config.filebrowserImageUploadUrl = '/ckeditor/pictures';

    // Rails CSRF token
    (function() {
        var csrf_token = $('meta[name=csrf-token]').attr('content'),
            csrf_param = $('meta[name=csrf-param]').attr('content');

        if (csrf_param !== undefined && csrf_token !== undefined) {
            config.filebrowserUploadUrl += "?" + csrf_param + "=" + encodeURIComponent(csrf_token);
            config.filebrowserImageUploadUrl += "?" + csrf_param + "=" + encodeURIComponent(csrf_token);
        }
    })();
};