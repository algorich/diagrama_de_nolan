# -*- encoding : utf-8 -*-
class Participant < ActiveRecord::Base
  attr_accessible :name, :email, :receive_email, :city_token, :country, :civil_liberty_score, :economic_freedom_score, :calculate_result

  has_many :responses, :dependent => :destroy
  has_many :questions, :through => :responses
  belongs_to :city

  validates_presence_of :name, :email, :country
  validates_presence_of :city_id, :if => :brazil?
  validate :validate_city_token, :if => :brazil?
  validates_format_of :email, :allow_blank => true,
    :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i

  POSITIONS = { 'Libertário' => 0, 'Esquerda' => 1, 'Centrista' => 2, 'Direita' => 3, 'Estatista' => 4 }

  attr_reader :city_token

  before_save :calculate_result, :if => lambda {
    self.civil_liberty_score and
    self.economic_freedom_score and
    self.position_result.blank?
  }

  before_save :set_city_name

  def set_city_name
    self.city_name = self.city.try(:name)
  end

  def brazil?
    self.country == 'Brazil'
  end

  def validate_city_token
    unless self.city_id
      errors.add(:city, I18n.t('activerecord.errors.messages.blank'))
    end
  end

  def city_token=(id)
    self.city_id = id
  end

  def receive_email_pretty
    { true => 'Sim', false => 'Não' }[self.receive_email]
  end

  def calculate_result
    centrist_positions = [
      [5, 6], [4, 3], [3, 4], [2, 5],
      [5, 3], [4, 4], [3, 5],
      [6, 3], [5, 4], [4, 5], [3, 6],
      [6, 4], [5, 5], [4, 6],
      [7, 4], [6, 5], [5, 6], [4, 7],
      [7, 5], [6, 6], [5, 7],
      [8, 5], [7, 6], [6, 7], [5, 8]
    ]

    c = self.civil_liberty_score
    e = self.economic_freedom_score

    if centrist_positions.include? [c, e]
      self.position_result = POSITIONS['Centrista']
    elsif c > 5 and e > 5
      self.position_result = POSITIONS['Libertário']
    elsif c > 5 and e < 6
      self.position_result = POSITIONS['Esquerda']
    elsif c < 6 and e > 5
      self.position_result = POSITIONS['Direita']
    elsif c < 6 and e < 6
      self.position_result = POSITIONS['Estatista']
    end
  end

  def self.statistics
    results = {}
    [:libertarian, :statist, :left, :right, :centrist].each do |position|
      results[position] = {:result => 0.0, :economic_freedom => 0.0, :civil_liberty => 0.0}
    end

    results[:total_civil_liberty] = 0.0
    results[:total_economic_freedom] = 0.0

    total = Participant.all.count

    if total > 0

      libertarians = Participant.where('position_result = ?', POSITIONS['Libertário'])
      statists = Participant.where('position_result = ?', POSITIONS['Estatista'])
      rights = Participant.where('position_result = ?', POSITIONS['Direita'])
      lefts = Participant.where('position_result = ?', POSITIONS['Esquerda'])
      centrists = Participant.where('position_result = ?', POSITIONS['Centrista'])

      total_libertarian = libertarians.count
      total_statist = statists.count
      total_right = rights.count
      total_left = lefts.count
      total_centrist = centrists.count

      results[:libertarian][:result] = (total_libertarian/total.to_f)*100
      results[:statist][:result] = (total_statist/total.to_f)*100
      results[:right][:result] = (total_right/total.to_f)*100
      results[:left][:result] = (total_left/total.to_f)*100
      results[:centrist][:result] = (total_centrist/total.to_f)*100

      libertarian_economic_freedom = libertarians.map(&:economic_freedom_score).reduce(&:+)
      statist_economic_freedom = statists.map(&:economic_freedom_score).reduce(&:+)
      right_economic_freedom = rights.map(&:economic_freedom_score).reduce(&:+)
      left_economic_freedom = lefts.map(&:economic_freedom_score).reduce(&:+)
      centrist_economic_freedom = centrists.map(&:economic_freedom_score).reduce(&:+)

      libertarian_civil_liberty = libertarians.map(&:civil_liberty_score).reduce(&:+)
      statist_civil_liberty = statists.map(&:civil_liberty_score).reduce(&:+)
      right_civil_liberty = rights.map(&:civil_liberty_score).reduce(&:+)
      left_civil_liberty = lefts.map(&:civil_liberty_score).reduce(&:+)
      centrist_civil_liberty = centrists.map(&:civil_liberty_score).reduce(&:+)

      results[:libertarian][:economic_freedom] = (libertarian_economic_freedom/total_libertarian.to_f)*10
      results[:statist][:economic_freedom] = (statist_economic_freedom/total_statist.to_f)*10
      results[:right][:economic_freedom] = (right_economic_freedom/total_right.to_f)*10
      results[:left][:economic_freedom] = (left_economic_freedom/total_left.to_f)*10
      results[:centrist][:economic_freedom] = (centrist_economic_freedom/total_centrist.to_f)*10

      results[:libertarian][:civil_liberty] = (libertarian_civil_liberty/total_libertarian.to_f)*10
      results[:statist][:civil_liberty] = (statist_civil_liberty/total_statist.to_f)*10
      results[:right][:civil_liberty] = (right_civil_liberty/total_right.to_f)*10
      results[:left][:civil_liberty] = (left_civil_liberty/total_left.to_f)*10
      results[:centrist][:civil_liberty] = (centrist_civil_liberty/total_centrist.to_f)*10

      [:libertarian, :statist, :right, :left, :centrist].each do |c|
        results[:total_civil_liberty] += eval("#{c}_civil_liberty")
        results[:total_economic_freedom] += eval("#{c}_economic_freedom")
      end

      results[:total_civil_liberty] = (results[:total_civil_liberty]/total.to_f)*10
      results[:total_economic_freedom] = (results[:total_economic_freedom]/total.to_f)*10
    end

    results
  end
end
