# -*- encoding : utf-8 -*-
class Ability
  include CanCan::Ability

  MODELS = [Configuration]

  def initialize(user)
    can :access, :rails_admin
    can :manage, :all
    cannot [:destroy, :create], Configuration
    cannot [:destroy, :create], Report
    cannot [:destroy, :create, :edit], Response
    cannot [:create, :edit], Participant
    cannot [:create, :destroy, :export], Page

    MODELS.each { |model| cannot :show, model }
  end
end

