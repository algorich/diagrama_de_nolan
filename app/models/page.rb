# -*- encoding : utf-8 -*-

class Page < ActiveRecord::Base
  validates_presence_of :title

  PAGES = {
    :lecture => 1,
    :libertarian_result => 2,
    :right_result => 3,
    :left_result => 4,
    :centrist_result => 5,
    :statist_result => 6,
    :about => 7,
    :donate => 9,
    :partners => 10
  }

  def title_by_lang lang
    if self.respond_to?("title_#{lang}")
      title_aux = self.send("title_#{lang}")

      return self.send("title_#{lang}") unless title_aux.blank?
    end

    return self.title
  end

  def content_by_lang lang
    if self.respond_to?("content_#{lang}")
      content_aux = self.send("content_#{lang}")

      return self.send("content_#{lang}") unless content_aux.blank?
    end

    return self.content
  end
end
