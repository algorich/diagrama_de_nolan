# -*- encoding : utf-8 -*-

class Report < ActiveRecord::Base

	def all_reports
		view = ActionView::Base.new(ActionController::Base.view_paths, {})
	  view.extend ApplicationHelper

		if self.name == 'Porcentagem de posições políticas'
	  	view.render(:partial => 'reports/show_participant')
		end
	end

end
