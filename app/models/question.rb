# -*- encoding : utf-8 -*-
class Question < ActiveRecord::Base
  has_many :responses
  has_many :participants, :through => :responses, :dependent => :destroy

  CATEGORY = { :economic_freedom => 1, :civil_liberties => 2 }

  validates_presence_of :question

  validates_presence_of :weight
  validates_presence_of :category

  def category_enum
    { 'Liberdade econômica' => 1, 'Liberdade civil' => 2 }
  end

  def question_by_lang lang
    if self.respond_to?("question_#{lang}")
      question_aux = self.send("question_#{lang}")

      return self.send("question_#{lang}") unless question_aux.blank?
    end

    return self.question
  end
end
