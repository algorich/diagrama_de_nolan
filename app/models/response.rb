# -*- encoding : utf-8 -*-
class Response < ActiveRecord::Base
  belongs_to :question
  belongs_to :participant

  ANSWERS = { :disagree => 2 , :perhaps => 1, :agree => 0 }

  validates_presence_of :response

  def response_enum
    ANSWERS.to_a
  end

  def self.answers_sorted_by_value
    ANSWERS.sort_by { |key, value| value }
  end

  def pretty_pt
    case self.response
      when 2
        'Discordo'
      when 1
        'Talvez'
      when 0
        'Concordo'
    end
  end
end
