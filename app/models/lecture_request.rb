# -*- encoding : utf-8 -*-
class LectureRequest
  include ActiveModel::Conversion
  include ActiveModel::Validations
  extend ActiveModel::Naming

  BRAZILIAN_STATES = ['AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO',
    'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN', 'RS',
    'RO', 'RR', 'SC', 'SP', 'SE', 'TO']

  attr_accessor :name, :email, :phone, :city, :state, :place

  validates_presence_of :name, :email, :phone, :city, :state, :place
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end

  def save
    if self.valid?
      LectureRequestMailer.lecture_request(:name => self.name,
        :email => self.email,
        :phone => self.phone,
        :city => self.city,
        :state => self.state,
        :place => self.place,
        :to => Configuration.first.email).deliver
      return true
    end
    false
  end
end
