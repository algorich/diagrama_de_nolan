# -*- encoding : utf-8 -*-

module SiteHelper

  def answers_input question_index
    out = ''
    Response.answers_sorted_by_value.each do |answer, value|
      out +=
      "<span class=\"#{answer.to_s}\"><strong>
        <input id=\"questionnaire_responses_question_#{question_index}_#{value}\" type=\"radio\" value=\"#{value}\" name=\"questionnaire[responses][question_#{question_index}]\">
        <label class=\"collection_radio_buttons\" for=\"questionnaire_responses_question_#{question_index}_#{value}\"><span>#{t('quiz.' + answer.to_s)}</span></label>
      </strong></span>"
    end
    return out.html_safe
  end
end
