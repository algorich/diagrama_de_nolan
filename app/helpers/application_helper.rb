# -*- encoding : utf-8 -*-
module ApplicationHelper
  def configuration
    @configuration ||= Configuration.first
  end

  def lookup
  	request.compatible_language_from(ActiveRecord::Base::Configuration.first.valid_langs)
  end
end
